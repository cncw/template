<?php

namespace App\Http\Controllers\CP\Slide;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Slide as Model;

class SlideController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.slide";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*')->orderBy('data_order','ASC')->get();
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
   
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $slide_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'en_title' =>   $request->input('en_title'),
                    'kh_title' =>   $request->input('kh_title'),
                    'creator_id' => $slide_id,
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $request->all(), 
                        [
                            
                           'en_title' => 'required',
                           'kh_title' => 'required'
                        ]);
        $image = FileUpload::uploadFile($request, 'image', 'uploads/slide');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $slide_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'en_title' =>   $request->input('en_title'),
                    'kh_title' =>   $request->input('kh_title'),
                    'updater_id' => $slide_id,
                    'updated_at' => $now
                );
        

        Validator::make(
        				$request->all(), 
			        	[
                            
                           'en_title' => 'required',
                           'kh_title' => 'required'
						]);

        $image = FileUpload::uploadFile($request, 'image', 'uploads/slide');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

     public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'Slide has been deleted'
        ]);
    }

    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Published status has been updated.'
      ]);
    }

}
