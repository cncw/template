<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Report as Report;

class ReportController extends FrontendController
{
    
    public function index($locale = "en") {
    	$data = Report::select('id',$locale.'_title as title','en_pdf','kh_pdf','created_at')->where('is_published',1)->orderBy('data_order','ASC')->paginate(10);

        $defaultData = $this->defaultData($locale);
        return view('frontend.report', ['locale'=>$locale,'defaultData'=>$defaultData,'data'=>$data]);
    }
}
