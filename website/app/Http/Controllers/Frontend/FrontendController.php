<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use App\Model\Gallary as Gallary;
use App\Model\Press as Press;
use App\Model\Document;
use App\Model\Video;
use App\Model\DocumentCategory;
use App\Model\Content;
use App\Model\PressCategory;
class FrontendController extends Controller
{
  	
	public $defaultData = array();
    public function __construct(){
      
    }

    public function defaultData($locale = "en"){
    	App::setLocale($locale);

        //Current Language
        $parameters = Route::getCurrentRoute()->parameters();

        $enRouteParamenters = $parameters;
        $enRouteParamenters['locale'] = 'en';
        $this->defaultData['enRouteParamenters'] = $enRouteParamenters;

        $khRouteParamenters = $parameters;
        $khRouteParamenters['locale'] = 'kh';
        $this->defaultData['khRouteParamenters'] = $khRouteParamenters;

        $this->defaultData['routeName'] = Route::currentRouteName();
        
        //$this->defaultData['lastest_news']  = Press::select('id',$locale.'_title as title',$locale.'_description as description','slug')->where(array('category_id'=>1,'is_featured'=>1))->limit(4)->get();
        $this->defaultData['lastest_news']  = Press::select('id',$locale.'_title as title',$locale.'_description as description','slug')->where(array('category_id'=>1,'is_featured'=>1))->limit(4)->get();
        $this->defaultData['videos']  = Video::select('id',$locale.'_title as title','url')->orderBy('created_at','DESC')->limit(3)->get();
        $this->defaultData['document_categories']  = DocumentCategory::select('id',$locale.'_title as title','slug')->orderBy('id','ASC')->get();
        $this->defaultData['press_categories']  = PressCategory::select('id',$locale.'_title as title','slug')->orderBy('id','ASC')->get();
        $this->defaultData['footer_address']  = Content::select('id',$locale.'_content as content')->where('slug','address')->first();
        $this->defaultData['footer_phone']  = Content::select('id',$locale.'_content as content')->where('slug','phone')->first();
        $this->defaultData['footer_email']  = Content::select('id',$locale.'_content as content')->where('slug','email')->first();
        $this->defaultData['footer_website']  = Content::select('id',$locale.'_content as content')->where('slug','website')->first();
        $this->defaultData['footer_copyright']  = Content::select('id',$locale.'_content as content')->where('slug','copyright')->first();
        //dd($this->defaultData['lastest_news']);
        return $this->defaultData;
    }
    

}
