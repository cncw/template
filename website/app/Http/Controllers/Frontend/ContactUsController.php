<?php

namespace App\Http\Controllers\Frontend;

use Session;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Client as Client;
use App\Model\Content;
use App\Model\Message;

class ContactUsController extends FrontendController
{
    
    public function index($locale = "en") {
    	$contact_address  = Content::select('id',$locale.'_content as content')->where('slug','address-contact')->first();
        $contact_phone  = Content::select('id',$locale.'_content as content')->where('slug','phone-contact')->first();
        $contact_email  = Content::select('id',$locale.'_content as content')->where('slug','email-contact')->first();
        $contact_website  = Content::select('id',$locale.'_content as content')->where('slug','website-contact')->first();

        $defaultData = $this->defaultData($locale);
        return view('frontend.contact-us', ['locale'=>$locale,'defaultData'=>$defaultData,'contact_address'=>$contact_address,'contact_phone'=>$contact_phone,'contact_email'=>$contact_email,'contact_website'=>$contact_website]);
    }

    public function store(Request $request , $locale = "")
    {
        $defaultData = $this->defaultData($locale);
        $now        = date('Y-m-d H:i:s');
        $data = array(
                    'name' =>   $request->input('name'),
                    'organization' =>  $request->input('organization'),
                    'position' =>  $request->input('position'),
                    'phone' =>  $request->input('phone'),
                    'subject' =>  $request->input('subject'), 
                    'email' =>  $request->input('email'),
                    'message' =>  $request->input('message'),
                    'created_at' => $now 
                );
         Session::flash('invalidData', $data );
         Validator::make(
                        $request->all(), 
                        [
                            'name' => 'required',
                            'organization' => 'required',
                            'position' => 'required',
                            'subject' => 'required',
                            'email' => 'email',
                            'message' => 'required',
                            'g-recaptcha-response' => 'required',
                        ])->validate();
        
        // Mail::to('koeun@gmail.com')->send(new NewUserWelcome($data));
        // $client_email = $request->input('email');
        // Mail::to($client_email)->send(new ReplyBackToClients($data)); 
        $id=Message::insertGetId($data);
        Session::flash('msg', __('contact_us.contact-successful-sent') );
        return redirect(route('contact-us', ['locale'=>$locale]));
    }
}
