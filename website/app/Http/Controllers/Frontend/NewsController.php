<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Press;
use App\Model\PressCategory;
class NewsController extends FrontendController
{
    
    public function index(Request $request, $locale = "",$page = '') {
    	//$page = $request->input('page');
    	

    	$category = PressCategory::select('id',$locale.'_title as title','slug')->where('slug',$page)->first();
        if($category){
    	$data = Press::select('id',$locale.'_title as title',$locale.'_description as description','image','slug','created_at')->where(array('category_id'=>$category->id,'is_published'=>1))->orderBy('data_order','ASC')->paginate(10);
        }else{
            $data = [];
        }
        $defaultData = $this->defaultData($locale);
        return view('frontend.news', ['locale'=>$locale,'defaultData'=>$defaultData,'category'=>$category,'data'=>$data,'page'=>$page]);
    }
    public function detail($locale = "",$page = '',$slug = '') {
    	
        $category = PressCategory::select('id',$locale.'_title as title','slug')->where('slug',$page)->first();
        if($category){
        $data = Press::select('id',$locale.'_title as title',$locale.'_description as description',$locale.'_content as content','image','slug','created_at')->where(array('category_id'=>$category->id,'slug'=>$slug))->first();
        }else{
            $data = [];
        }

        $defaultData = $this->defaultData($locale);
        return view('frontend.news-detail', ['locale'=>$locale,'defaultData'=>$defaultData,'category'=>$category,'data'=>$data,'page'=>$page]);
    }
    public function annonDetail($locale = "en") {
    	
        $defaultData = $this->defaultData($locale);
        return view('frontend.annon-detail', ['locale'=>$locale,'defaultData'=>$defaultData]);
    }
}
