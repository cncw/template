<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Client as Client;
use App\Model\Content as Content;
use App\Model\Document;
use App\Model\DocumentCategory;

class LawAndRegulationController extends FrontendController
{
    
    public function index($locale = "",$slug = '') {
    	
    	//$slug = $request->input('slug');

    	$category = DocumentCategory::select('id',$locale.'_title as title','slug')->where('slug',$slug)->first();
    	if($category){
    		$data = Document::select('id',$locale.'_title as title','en_pdf','kh_pdf','image','created_at')->where(array('category_id'=>$category->id,'is_published'=>1))->paginate(10);
    	}else{
    		$data = [];
    	}
    	

        $defaultData = $this->defaultData($locale);
        return view('frontend.law-and-regulation', ['locale'=>$locale,'defaultData'=>$defaultData,'slug'=>$slug,'data'=>$data,'category'=>$category]);
    }
}
