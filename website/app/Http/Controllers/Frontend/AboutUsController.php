<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Client as Client;
use App\Model\Content as Content;
use App\Model\PageSlide as PageSlide;

class AboutUsController extends FrontendController
{
    
    public function index(Request $request, $locale = "en") {
    	$slug = $request->input('page');
    	
    	$data = Content::select('id',$locale.'_name as name',$locale.'_content as content')->where('slug',$slug)->first();
        $defaultData = $this->defaultData($locale);
        return view('frontend.about-us', ['locale'=>$locale,'defaultData'=>$defaultData,'data'=>$data]);
    }
}
