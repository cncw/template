<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Slide;
use App\Model\Content as Content;
use App\Model\Press as Press;
use App\Model\DocumentCategory;
use App\Model\Document;

class HomeController extends FrontendController
{
    
    public function index($locale = "") {
    	
    	$slides           = Slide::select('id','image',$locale.'_title as title')->orderBy('data_order','ASC')->where('is_published',1)->get();
    	$announcements = Press::select('id',$locale.'_title as title',$locale.'_description as description','image','slug','created_at')->where(array('category_id'=>3,'is_featured'=>1))->limit(3)->get();

    	$presses = Press::select('id',$locale.'_title as title',$locale.'_description as description','image','slug','created_at')->where(array('category_id'=>1,'is_featured'=>1))->limit(3)->get();

    	$document_categories           = DocumentCategory::select('id',$locale.'_title as title')->orderBy('id','ASC')->get();
    	$royal_decrees = Document::select('id',$locale.'_title as title','en_pdf','kh_pdf','image','created_at')->where(array('category_id'=>1,'is_published'=>1))->limit(3)->get();
    	$sub_degrees = Document::select('id',$locale.'_title as title','en_pdf','kh_pdf','image','created_at')->where(array('category_id'=>2,'is_published'=>1))->limit(3)->get();
    	$text_announcements = Document::select('id',$locale.'_title as title','en_pdf','kh_pdf','image','created_at')->where(array('category_id'=>3,'is_published'=>1))->limit(3)->get();
    	$decisions = Document::select('id',$locale.'_title as title','en_pdf','kh_pdf','image','created_at')->where(array('category_id'=>4,'is_published'=>1))->limit(3)->get();
    	$onthers = Document::select('id',$locale.'_title as title','en_pdf','kh_pdf','image','created_at')->where(array('category_id'=>5,'is_published'=>1))->limit(3)->get();

        $defaultData = $this->defaultData($locale);
        return view('frontend.home', ['locale'=>$locale,'defaultData'=>$defaultData,'slides'=>$slides,'presses'=>$presses,'announcements'=>$announcements,'document_categories'=>$document_categories,'royal_decrees'=>$royal_decrees,'sub_degrees'=>$sub_degrees,'text_announcements'=>$text_announcements,'decisions'=>$decisions,'onthers'=>$onthers]);
    }

    public function minister($locale = ""){
        $defaultData = $this->defaultData($locale);
        $bio = Content::select('id',$locale.'_name as name',$locale.'_content as content')->where('slug','message-from-prie-minister')->first();
        $spech = Content::select('id',$locale.'_name as name',$locale.'_content as content')->where('slug','message-from-minister')->first();
        return view('frontend.minister', ['locale'=>$locale,'defaultData'=>$defaultData,'bio'=>$bio,'spech'=>$spech]);
    }
}
