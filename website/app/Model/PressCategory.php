<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class PressCategory extends Model
{
   
    protected $table = 'press_categories';
   
    public function presses(){
        return $this->hasMany('App\Model\Press','category_id');
    }
}
