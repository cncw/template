<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
   
    protected $table = 'document_categories';
   
    public function documents(){
        return $this->hasMany('App\Model\Document','category_id');
    }
}
