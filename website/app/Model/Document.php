<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
   
    protected $table = 'documents';
   
    public function category(){
        return $this->belongsTo('App\Model\DocumentCategory','category_id');
    }
}
