<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Press extends Model
{
   
    protected $table = 'presses';
   
    public function category(){
        return $this->belongsTo('App\Model\PressCategory','category_id');
    }
}
