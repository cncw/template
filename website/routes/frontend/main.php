<?php
Route::get('{locale}/home', 								[ 'as' => 'home',					'uses' => 'HomeController@index']);
Route::get('{locale}/about-us', 							[ 'as' => 'about-us',					'uses' => 'AboutUsController@index']);
Route::get('{locale}/news-detail/{page}/{slug}', 				[ 'as' => 'news-detail',					'uses' => 'NewsController@detail']);
Route::get('{locale}/annon-detail', 				[ 'as' => 'annon-detail',					'uses' => 'NewsController@annonDetail']);
Route::get('{locale}/law-and-regulation/{slug}', 				[ 'as' => 'law-and-regulation',					'uses' => 'LawAndRegulationController@index']);
Route::get('{locale}/news-and-activities/{page}', 				[ 'as' => 'news-and-activities',					'uses' => 'NewsController@index']);
Route::get('{locale}/report', 							[ 'as' => 'report',					'uses' => 'ReportController@index']);
Route::get('{locale}/contact-us', 						[ 'as' => 'contact-us',					'uses' => 'ContactUsController@index']);
Route::put('{locale}/submit-contact', 						[ 'as' => 'submit-contact',					'uses' => 'ContactUsController@store']);
Route::get('{locale}/minister', 							[ 'as' => 'minister',					'uses' => 'HomeController@minister']);