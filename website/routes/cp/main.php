<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' => 'auth', 'namespace' => 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		Route::group(['as' => 'user.',  'prefix' => 'user', 'namespace' => 'User'], function () {
			require(__DIR__.'/user.php');
		});
		
		Route::group(['as' => 'content.',  'prefix' => 'content', 'namespace' => 'Content'], function () {
			require(__DIR__.'/content.php');
		});
		Route::group(['as' => 'message.',  'prefix' => 'message', 'namespace' => 'Message'], function () {
			require(__DIR__.'/message.php');
		});
		Route::group(['as' => 'slide.',  'prefix' => 'slide', 'namespace' => 'Slide'], function () {
			require(__DIR__.'/slide.php');
		});
	
		
		Route::group(['as' => 'service.',  'prefix' => 'service', 'namespace' => 'Service'], function () {
			require(__DIR__.'/service.php');
		});
		Route::group(['as' => 'image.',  'prefix' => 'image', 'namespace' => 'Image'], function () {
			require(__DIR__.'/image.php');
		});

		Route::group(['as' => 'video.',  'prefix' => 'video', 'namespace' => 'Video'], function () {
			require(__DIR__.'/video.php');
		});
		Route::group(['as' => 'report.',  'prefix' => 'report', 'namespace' => 'Report'], function () {
			require(__DIR__.'/report.php');
		});
		Route::group(['as' => 'law.',  'prefix' => 'law-and-regulation', 'namespace' => 'Law'], function () {
			require(__DIR__.'/law.php');
		});
		Route::group(['as' => 'news.',  'prefix' => 'news', 'namespace' => 'News'], function () {
			require(__DIR__.'/news.php');
		});
		Route::group(['as' => 'announcement.',  'prefix' => 'announcement', 'namespace' => 'Announcement'], function () {
			require(__DIR__.'/announcement.php');
		});
		Route::group(['as' => 'activity.',  'prefix' => 'activity', 'namespace' => 'Activity'], function () {
			require(__DIR__.'/activity.php');
		});
		
	});