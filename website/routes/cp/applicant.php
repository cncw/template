<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'ApplicantController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'ApplicantController@create']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ApplicantController@edit']);
Route::post('/', 				['as' => 'update', 			'uses' => 'ApplicantController@update']);
Route::put('/', 				['as' => 'store', 			'uses' => 'ApplicantController@store']);
Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ApplicantController@trash']);

