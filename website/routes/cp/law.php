<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Award

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'LawController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'LawController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'LawController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'LawController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'LawController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'LawController@trash']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'LawController@updateStatus']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'LawController@order']);
});	