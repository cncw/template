<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Award

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ReportController@index']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ReportController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ReportController@update']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ReportController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ReportController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ReportController@trash']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ReportController@updateStatus']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ReportController@order']);
});	