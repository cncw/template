<?php

use Illuminate\Database\Seeder;

class DocumentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('document_categories')->insert(
            [
                [
                    'slug'       => 'royal-decree',
                    'en_title'       => 'Royal Decree',
                    'kh_title'       => 'ព្រះរាជក្រឹត្យ',
                ], 
                [
                    'slug'       => 'sub-degree',
                    'en_title'       => 'Sub Degree',
                    'kh_title'       => 'អនុក្រឹត្យ',
                ],
                [
                    'slug'       => 'annountment',
                    'en_title'       => 'Annountment',
                    'kh_title'       => 'សេចក្តីប្រកាស',
                ],
                [
                    'slug'       => 'decision',
                    'en_title'       => 'Decision',
                    'kh_title'       => 'សេចក្តីសម្រេច',
                ],
                [
                    'slug'       => 'other',
                    'en_title'       => 'Other',
                    'kh_title'       => 'ឯកសារផ្សេងៗ',
                ]
            ]
        );
	}
}
