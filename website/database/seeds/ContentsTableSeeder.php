<?php

use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('contents')->insert(
            [
            
                
                [
                    'page'              => 'About Us',
                    'slug'              => 'history', 
                    'en_name'           => 'Under Construction',
                    'kh_name'           => 'Under Construction',     
                    'en_content'        => 'Under Construction',
                    'kh_content'        => 'Under Construction',
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                [
                    'page'              => 'About Us',
                    'slug'              => 'mission', 
                    'en_name'            => 'Mission',
                    'kh_name'           => 'Mission',     
                    'en_content'        => 'Under Construction',
                    'kh_content'        => 'Under Construction',
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                [
                    'page'              => 'About Us',
                    'slug'              => 'vision', 
                    'en_name'            => 'Vision',
                    'kh_name'           => 'Vision',     
                    'en_content'        => 'Under Construction',
                    'kh_content'        => 'Under Construction',
                    'image_required'    => 0,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                [
                    'page'              => 'General',
                    'slug'              => 'message-from-prie-minister', 
                    'en_name'            => 'Message From Prie Minister',
                    'kh_name'           => 'Message From Prie Minister',     
                    'en_content'        => 'Under Construction',
                    'kh_content'        => 'Under Construction',
                    'image_required'    => 1,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ],
                [
                    'page'              => 'General',
                    'slug'              => 'message-from-minister', 
                    'en_name'            => 'Message From Minister',
                    'kh_name'           => 'Message From Minister',     
                    'en_content'        => 'Under Construction',
                    'kh_content'        => 'Under Construction',
                    'image_required'    => 1,
                    'image'             => '',
                    'width'             => 0,
                    'height'            => 0,
                    'editor_required'   => 1,
                    'updater_id'        => 1,
                ]
            ]
        );
	}
}
