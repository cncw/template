<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User
        $this->call(UsersTableSeeder::class);
        $this->call(SlideTableSeeder::class);
        $this->call(VideoTableSeeder::class);
        $this->call(PressCategoriesTableSeeder::class);
        $this->call(DocumentCategoriesTableSeeder::class);
        $this->call(PressTableSeeder::class);
       
    }
}
