<?php

use Illuminate\Database\Seeder;

class PressCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('press_categories')->insert(
            [
                [
                    'slug'       => 'news',
                    'en_title'       => 'News',
                    'kh_title'       => 'ពត័មាន',
                ], 
                [
                    'slug'       => 'activities',
                    'en_title'       => 'Activities',
                    'kh_title'       => 'សម្មភាព',
                ],
                [
                    'slug'       => 'announcement',
                    'en_title'       => 'Announcement',
                    'kh_title'       => 'សេចក្តីជូនដំណឹង',
                ]
            ]
        );
	}
}
