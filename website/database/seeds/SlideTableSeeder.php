<?php

use Illuminate\Database\Seeder;

class SlideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('slides')->insert(
            [
                [
                    'en_title'       => 'Slide 1',
                    'kh_title'       => 'Slide 1',
                    'image'          => 'public/frontend/images/slide2.jpg',
                    'is_published'   =>    1,
                ], 
                [
                    'en_title'       => 'Slide 2',
                    'kh_title'       => 'Slide 2',
                    'image'          => 'public/frontend/images/slide1.jpg',
                    'is_published'   =>    1,
                ]
            ]
        );
	}
}
