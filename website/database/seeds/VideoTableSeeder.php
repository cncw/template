<?php

use Illuminate\Database\Seeder;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	   DB::table('videos')->insert(
            [
                [
                    'en_title'       => 'Investment',
                    'kh_title'       => 'Investment',
                    'url'          => 'ntnIlXRCbwg',
                    'is_published'   =>    1,
                ],
                [
                    'en_title'       => 'Marketting',
                    'kh_title'       => 'Marketting',
                    'url'          => 'LqcKuOix4-k',
                    'is_published'   =>    1,
                ]

            ]
        );
	}
}
