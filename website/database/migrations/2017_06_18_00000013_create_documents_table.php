<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_order')->unsigned()->index()->nullable();
			$table->integer('category_id')->unsigned()->index()->nullable();
            $table->foreign('category_id')->references('id')->on('document_categories')->onDelete('cascade');
            $table->string('slug', 500)->default('');
            $table->string('kh_title', 500);
            $table->string('en_title', 500);
            $table->boolean('is_published')->default(0);
            $table->string('en_pdf', 550)->default('');
            $table->string('kh_pdf', 550)->default('');
            $table->string('image', 550)->default('');
            $table->integer('num_of_views')->default(0)->unsigned()->index()->nullable();
            
            $table->integer('deleter_id')->default(1)->unsigned()->index()->nullable();
            $table->integer('creator_id')->default(1)->unsigned()->index()->nullable();
            $table->integer('updater_id')->default(1)->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
