<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('data_order')->unsigned()->index()->nullable();
            $table->string('slug', 500);
            $table->string('kh_title', 500);
            $table->string('en_title', 500);
            $table->boolean('is_published')->default(0);
            $table->string('en_pdf', 550)->default('');
            $table->string('kh_pdf', 550)->default('');
            $table->integer('num_of_views')->default(0)->unsigned()->index()->nullable(); 
            $table->integer('deleter_id')->default(1)->unsigned()->index()->nullable();
            $table->integer('creator_id')->default(1)->unsigned()->index()->nullable();
            $table->integer('updater_id')->default(1)->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
