<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
    'message-from-president' => 'Message from President',
    'home' => 'Home',
    'product' => 'Product',
    'news' => 'News', 
    'gallary' => 'Gallery',
    'pictures' => 'Pictures', 
    'videos' => 'Videos', 
    'career' => 'Career', 
    'about-us' => 'About Us',   
    'history' => 'History', 
    'mission' => 'Mission', 
    'vision' => 'Vision', 
    'law-and-regulation' => 'Law and Regulation', 
    'news-and-activities' => 'News and Activities', 
    'report' => 'Report',
    'send-message' => 'Send Message',
    'no-data-here' => 'No Data here',

    'view-more' => 'View More', 
    'download' => 'Download',
    'lastest-news' => 'Lastest News',
    'annoucement' => 'Announcement',
    'read-more' => 'Read More',
    'minister' => 'Minister',
    'video' => 'Video',
    'contact-us' => 'Contact us',
    
    'management-team' => 'Management Team', 
    'job-announcement' => 'Job Announcement', 
    'contact-us' => 'Contact Us', 
    'mon-sat' => 'Mon-Sat', 
    'sat' => 'Sat', 
    'sun' => 'Sun', 
    'closed' => 'Closed',
    'contact-information' => 'Contact Information',
    'address' => '#281, Street 105K, Sangkat Kakab, Khan Porthisenchey, Phnom Penh, Kingdom of Cambodia.',
    'opening-hours' => 'Opening Hours',
    'useful-links' => 'Useful Links',
    'connect-with-us' => 'Connect With Us',
    'copyright' => 'Copyright ©2017 LyLy Food. All Rights Reserved',
//============= General
   'name' => 'Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'subject' => 'Subject',
    'about-us' => 'About Us',
    'welcome' => 'Welcome to Ministry of Public Work And Transpotation',
    'all-right' => '© 2017 MPWT - Ministry of Public Works and Transport. All Right Reserved.',

    'send-message' => 'Sent Message',
    'your-email' => 'Your Email',
    'your-phone' => 'Your Phone',
    'message' => 'Type Your Message Here . . .',
    'your-subject' => 'Subject',
    'position' => 'Position',
    'your-name' => 'Your Name',
    'your-organization' => 'Your Organization',
    'your-name' => 'Your Name',
            //======================== Contact Us
            'any-questions' => 'Get In Touch If You Have Any Questions',
            'send-us-message' => 'Send Us Message',
            'interested' => 'If you are interested in us and willing to make inquiry, please feel free to contact use by form below..',
            'full-name' => 'Full Name',
            //'email' => 'Email',
            
            
            'send' => 'Send',
            'enter-your-name' => 'Enter your name',
            'enter-your-email' => 'Enter your email',
            'enter-your-subject' => 'Enter your subject',
            'enter-your-phone' => 'Enter your phone numeber',
            'enter-your-organization' => 'Enter your organization',
            'enter-your-position' => 'Enter your position',
            'enter-your-message' => 'Enter your message',

            'contact-successful-sent' => 'Your request has been Sent! We will contact back soon.',


           
            

    //============================ Validation
    'enter-your-phone' => 'Please enter your phone.',
    'enter-position' => 'Please enter the position you want to apply.',
    'chose-pdf' => 'Please choose only pdf.',
    'errorname' => 'Please enter your Name.',
    'errorposition' => 'Please enter your position.',
    'errorphone' => 'Please enter your Phone.',
    'errororganization' => 'Please enter your Organization.',
    'errorsubject' => 'Please enter your subject.',
    'erroremail' => 'Please enter your e-mail.',
    'incorrectemail' => 'Your email is incorrect! Please enter your e-mail.',
    'errormessage' => 'Please write down your inquiry.',
    'errorrecaptcha' => 'Please click recaptcha below.',
    'sorry'  => 'Sorry! There is an error existed.',
    'erorrmajor'=>'Please tell us your major',
    'erorrcomunication'=>'Please tell us your purpose',
    'comunication'=>'Purpose',
];
