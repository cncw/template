<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    //============ Manu
    'message-from-president' => 'សារពីមន្រ្តីក្រសួង',
    'home' => 'ទំព័រដើម',
    'product' => 'ផលិតផល',
    'news' => 'ព័ត៌មាន', 
    'gallary' => 'វិចិត្រសាល',
    'pictures' => 'រូបភាព', 
    'videos' => 'វីដេអូ', 
    'career' => 'ជ្រើសរើសបុគ្គលិក', 
    'about-us' => 'អំពី​ពួក​យើង',   
    'history' => 'ប្រវត្តិ', 
    'mission' => 'បេសកកម្ម', 
    'vision' => 'ទស្សនៈវិស័យ', 
    'law-and-regulation' => 'ច្បាប់និងបទប្បញ្ញត្តិ', 
    'news-and-activities' => 'ព័ត៌មាននិងសកម្មភាព', 
    'report' => 'របាយការណ៍',
    'send-message' => 'ផ្ញើ​សារមកកាន់យើង',
    'no-data-here' => 'គ្មានទិន្នន័យនៅទីនេះ',

    'view-more' => 'មើល​ច្រើន​ទៀត', 
    'download' => 'ទាញយក',
    'lastest-news' => 'ព័ត៌មានថ្មីៗ',
    'annoucement' => 'សេចក្តីជូនដំណឹង',
    'read-more' => 'អាន​បន្ថែម',
    'minister' => 'រដ្ឋមន្រ្តី',
    'video' => 'ព័ត៌មានជាវីដេអូ',
    'contact-us' => 'ទំនាក់ទំនងយើង',
    
    'management-team' => 'ក្រុម​អ្នកគ្រប់គ្រង', 
    'job-announcement' => 'ជ្រើសរើបគ្គុលិត្ត', 
    'contact-us' => 'ទាក់ទង​មក​ពួក​យើង', 
    'mon-sat' => 'ច័ន្ទ - សៅរ៍',
    'sat' => 'សៅរ៍', 
    'sun' => 'អាទិត្យ', 
    'closed' => 'បិទ', 
    'contact-information' => 'ព័ត៌មានទំនាក់ទំនង',
    'address' => 'អគារលេខ ២៨១ ផ្លូវ ១០៥កា សង្កាត់កាកាប ខណ្ឌពោធិ៍សែនជ័យ រាជធានីភ្នំពេញ ព្រះរាជាណាចក្រកម្ពុជា',
    'opening-hours' => 'បើកម៉ោង',
    'useful-links' => 'ទំព័រកាត់',
    'connect-with-us' => 'ទាក់ទងជាមួយយើង',
    'copyright' => 'រក្សាសិទ្ធិឆ្នាំ 2017 LyLy Food។ រក្សា​រ​សិទ្ធ​គ្រប់យ៉ាង',
//============= General
    'name' => 'ឈ្មោះ',
    'phone' => 'ទូរស័ព្ទ',
    'email' => 'អីុម៉ែល',
    'about-us' => 'អំពីយើង',
    'subject' => 'ប្រធានបទ',
    'welcome' => 'ស្វាគមន៍មកកាន់ក្រសួងសាធារណការនិងដឹកជញ្ជូន',
    'all-right' => '© 2017 MPWT - ក្រសួងសាធារណការនិងដឹកជញ្ជូន។ សិទ្ធិគ្រប់យ៉ាង។',

    'send-message' => 'ផ្ញើសារមកពួកយើង',
    'your-email' => 'អីុម៉ែលរបស់​អ្នក',
    'your-phone' => 'ទូរស័ព្ទ​របស់​អ្នក',
    'message' => 'បញ្ចូលសាររបស់អ្នកនៅទីនេះ . . .',
    'your-subject' => 'ប្រធានបទ',
    'position' => 'មុខដំណែង',
    'your-name' => 'ឈ្មោះ​របស់​អ្នក',
    'your-organization' => 'អង្គការរបស់អ្នក',
    'your-name' => 'ឈ្មោះ​របស់​អ្នក',
            //======================== Contact Us
            'any-questions' => 'Get In Touch If You Have Any Questions',
            'send-us-message' => 'Send Us Message',
            'interested' => 'If you are interested in us and willing to make inquiry, please feel free to contact use by form below..',
            'full-name' => 'Full Name',
            //'email' => 'Email',
            
            
            'send' => 'Send',
            'enter-your-name' => 'បញ្ចូល​ឈ្មោះ​របស់​អ្នក',
            'enter-your-email' => 'បញ្ចូលអ៊ីម៉ែលរបស់អ្នក',
            'enter-your-subject' => 'បញ្ចូលប្រធានបទរបស់អ្នក',
            'enter-your-phone' => 'បញ្ចូលលេខទូរស័ព្ទរបស់អ្នក',
            'enter-your-organization' => 'បញ្ចូលអង្គភាពរបស់អ្នក',
            'enter-your-position' => 'បញ្ចូលមុខតំណែងរបស់អ្នក',
            'enter-your-message' => 'បញ្ចូលសាររបស់អ្នក',

            'contact-successful-sent' => 'សំណើរបស់អ្នកត្រូវបានផ្ញើ! យើងនឹងទាក់ទងមកវិញក្នុងពេលឆាប់ៗ។',


    //============================ Validation
    'enter-your-phone' => 'Please enter your phone.',
    'enter-position' => 'Please enter the position you want to apply.',
    'errorphone' => 'សូមបញ្ចូលទូរស័ព្ទរបស់អ្នក។',
    'errorposition' => 'សូមបញ្ចូលទីតាំងរបស់អ្នក។',
    'errororganization' => 'សូមបញ្ចូលអង្គភាពររបស់អ្នក។',
    'errorname' => 'សូមបញ្ចូលឈ្មោះរបស់អ្នក។',        
    'errorsubject' => 'សូមបញ្ចូលប្រធានបទរបស់អ្នក។',
    'erroremail' => 'សូមបញ្ចូលអ៊ីម៉ែលរបស់អ្នក។',
    'incorrectemail' => 'អ៊ីមែលរបស់អ្នកមិនត្រឹមត្រូវ! សូមបញ្ចូលអ៊ីម៉ែលរបស់អ្នក។',
    'errormessage' => 'សូមសរសេរសំណួររបស់អ្នក។',
    'errorrecaptcha' => 'សូមចុច recaptcha ខាងក្រោម។',
    'sorry'  => 'សុំទោស! មានកំហុសកើតឡើង។',
    'erorrmajor'=>'សូមប្រាប់មុខវិជ្ជារបស់អ្នក',
    'erorrcomunication'=>'សូមប្រាប់យើងនូវគោលបំណងរបស់អ្នក',
    'comunication'=>'គោលបំណង',
];
