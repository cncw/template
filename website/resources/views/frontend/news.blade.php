@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-news-and-activities', 'current-menu-item')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
      <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post -->
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div id="tab-prokas" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
               <div class="news-box  base-box">
                <header class="nb-header" >
                  <h2 class="nb-title" style=";"><span >@if($category) {{$category->title}} @endif</span></h2>
                </header> <!--nb header-->
                @if($data)  
                <div class="nb-content">
                  <div class="news-list ">
                    @foreach($data as $row)
                    <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                      <div class="news-image">
                        <a href="{{route('news-detail',['locale'=>$locale,'page'=> $page,'slug'=>$row->slug])}}"><img src="{{ asset ($row->image)}}" alt="{{$row->title}}" width="190" height="122"></a><span class="post-format-icon"></span>
                      </div>
                      <div class="news-summary has-feature-image">
                        <h3><a href="{{route('news-detail',['locale'=>$locale,'page'=> $page,'slug'=>$row->slug])}}">{{$row->title}}<!-- <img src="{{ asset ('public/frontend/images/news.gif')}}" /> </a> --></h3>
                        <div class="mom-post-meta nb-item-meta">
                          <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                       
                        </div> <!--meta-->
                        <P>{{str_limit($row->description,70)}}          <a href="{{route('news-detail',['locale'=>$locale,'page'=> $page,'slug'=>$row->slug])}}" class="read-more-link">{{__('general.read-more')}} <i class="fa fa-angle-double-right"></i></a></P>
                      </div>
                    </article>
                    @endforeach
                    <div class="pad10">
                      <div class="text-center">  
                        @if($data)
                          {{ $data->links('vendor.pagination.frontend-html') }}
                        @endif
                      </div> 
                    </div>
                  </div> <!--news list-->
                </div>
                @else
                   <p style="text-align: center;padding-top: 5px;">{{__('general.no-data-here')}}</p>
                @endif

              </div> 
            </div> 
          </div>
        </div> <!--main container-->            
        
        <!-- Sidebar -->
        
        @include('frontend.sidebar.related_post')
        
        <!-- EndSidebar -->

    </div> <!--content boxed wrapper-->
     


@endsection