@extends('frontend/layouts.master')

@section('title', 'Home | Department of Good Govener')
@section('active-home', 'current-menu-item')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
      <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post -->
      <div class="inner">
        <div class="main_container">
            <div class="main-col">

              <div class="vc_row wpb_row vc_row-fluid"  >
                <div class="vc_col-sm-12 wpb_column vc_column_container ">
                  <div class="wpb_wrapper">
                    <div class="feature-slider base-box nav-thumbs fs-wide new-style  " data-speed="600" data-timeout="4000" data-sanimation="" data-easing="easeInOutCubic" data-rndn="182" data-animation_new="slide" data-animation_in="" data-animation_out="" data-autoplay="true" data-items="6">
                      <div class="mom_visibility_mobile fs-drection-nav fs-dnav-182">
                        <span class="fsd-prev"><i class="fa-icon-angle-left"></i></span>
                        <span class="fsd-next"><i class="fa-icon-angle-right"></i></span>
                      </div>
                      <div class="fslides fs-182">

                        @foreach($slides as $row)
                        <div class="fslide post-3062 post type-post status-publish format-image category-silde post_format-post-format-image" itemscope itemtype="http://schema.org/Article" data-i="10">
                            <a href="#"><img src="{{ asset ($row->image)}}" alt="" width="822" height="512"></a>
                            <div class="slide-caption fs-caption-alt nav-is-thumbs">
                              <h2 style="font-size:20px;"><a href="#">{{$row->title}}</a></h2>
                            </div>
                        </div>
                        @endforeach
                       <!--  <div class="fslide post-3062 post type-post status-publish format-image category-silde post_format-post-format-image" itemscope itemtype="http://schema.org/Article" data-i="10">
                            <a href="#"><img src="{{ asset ('public/frontend/images/slide2.jpg')}}" alt="" width="822" height="512"></a>
                            <div class="slide-caption fs-caption-alt nav-is-thumbs">
                              <h2 style="font-size:20px;"><a href="#">a</a></h2>
                            </div>
                        </div> -->
                      </div>
                   
                    </div> <!--fearure slider-->

                  </div> 
                </div> 
              </div> 

              <div class="vc_row wpb_row vc_row-fluid"  >
                <div class="vc_col-sm-12 wpb_column vc_column_container ">
                  <div class="wpb_wrapper">      
                    <div class="wpb_tabs main_tabs base-box wpb_content_element" data-interval="0">
                      <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
                        <ul class="tabs ui-tabs-nav vc_clearfix">
                          <li><a href="#tab-news">{{__('general.annoucement')}}</a></li>
                        </ul>
                        <div class="tabs-content-wrap">         
                          <div id="tab-news" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <header class="nb-header" >
                                <h2 class="nb-title" style=";"><span >{{__('general.annoucement')}}</span></h2>
                              </header> <!--nb header-->
                              <div class="nb-content">
                                <div class="news-list ">
                                  @foreach($announcements as $row)
                                  <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-image">
                                      <a href="{{route('news-detail',['locale'=>$locale,'page'=> 'annoucement','slug'=>$row->slug])}}"><img src="{{ asset ($row->image)}}" alt="កិច្ចប្រជុំដើម្បីត្រៀមរៀបចំអនុវត្តផែនការយុទ្ធសាស្រ្តជាតិស្តីពីអត្តសញ្ញាណកម្ម ឆ្នាំ២០១៧-២០២៦" width="190" height="122"></a>
                                    </div>
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="{{route('news-detail',['locale'=>$locale,'page'=> 'annoucement','slug'=>$row->slug])}}">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        
                                      </div> <!--meta-->
                                      <P>{{str_limit($row->description,70)}}</P>         
                                       <a href="{{route('news-detail',['locale'=>$locale,'page'=> 'annoucement','slug'=>$row->slug])}}" class="read-more-link"> {{__('general.read-more')}} <i class="fa fa-angle-double-right"></i></a></P>
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('news-and-activities',['locale'=>$locale,'page'=>'annoucement'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div>   
                                </div> <!--news list-->
                              </div>
                                            
                            </div> 
                          
                          </div> 
                         
                        </div>

                      </div>
                      <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
                        <ul class="tabs ui-tabs-nav vc_clearfix">
                          <li><a href="#tab-news">{{__('general.lastest-news')}}</a></li>
                        </ul>
                        <div class="tabs-content-wrap">         
                          <div id="tab-news" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <header class="nb-header" >
                                <h2 class="nb-title" style=";"><span >{{__('general.lastest-news')}}</span></h2>
                              </header> <!--nb header-->
                              <div class="nb-content">
                                <div class="news-list ">
                                 
                              ​​​​  @foreach($presses as $row)
                                  <article class="nl-item post-4187 post type-post status-publish format-standard has-post-thumbnail category-new-02" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-image">
                                      <a href="{{route('news-detail',['locale'=>$locale,'page'=> 'news','slug'=>$row->slug])}}"><img src="{{ asset ($row->image)}}" data-hidpi="http://news.gdi.gov.kh/wp-content/uploads/2017/01/01-610x380.jpg" alt="ច្ចប្រជុំបូកសរុបលទ្ធផលការងារបញ្រ្ជាបយេនឌ័រប្រចាំឆ្នាំ២០១៦ និងលើកទិសដៅអនុវត្តការងារឆ្នាំ២០១៧" width="190" height="122"></a>
                                    </div>
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="{{route('news-detail',['locale'=>$locale,'page'=> 'news','slug'=>$row->slug])}}">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-01-20T11:36:21+00:00" class="entry-date"> {{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        
                                      </div> <!--meta-->
                                      <p> {{str_limit($row->description,70)}}<a href="{{route('news-detail',['locale'=>$locale,'page'=> 'news','slug'=>$row->slug])}}" class="read-more-link"> {{__('general.read-more')}} <i class="fa fa fa-angle-double-right"></i></a></p>
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('news-and-activities',['locale'=>$locale,'page'=>'news'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div> 
                                </div> <!--news list-->
                              </div>
                                            
                            </div> <!--news box-->
                          </div> 
                        </div>
                      </div>
                      <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
                        <ul class="tabs ui-tabs-nav vc_clearfix">
                          <!-- <li><a href="#tab-news">ព្រះ រាជក្រឹត្យ</a></li>
                          <li><a href="#tab-news1">អនុក្រឹត្យ</a></li>
                          <li><a href="#tab-news">សេចក្តីប្រកាស</a></li>
                          <li><a href="#tab-news">សេចក្តីសម្រេច</a></li>
                          <li><a href="#tab-news">ឯកសារផ្សេងៗ</a></li> -->
                          @foreach($document_categories as $row)
                             <li><a href="#tab-news-{{$row->id}}">{{$row->title}}</a></li>
                          @endforeach
                        </ul>
                        <div class="tabs-content-wrap">         
                          <div id="tab-news-1" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <div class="nb-content">
                                <div class="news-list ">
                                  @foreach($royal_decrees as $row)
                                  <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="#">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        <span class="wrap_download" style="font-size: 18px;float:right;color: #29292996;">{{__('general.download')}}: <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">EN</a> / <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">KH</a></span>
                                        
                                      </div> <!--meta-->
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('law-and-regulation',['locale'=>$locale,'slug'=>'royal-decree'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div> 
                                </div> <!--news list-->
                              </div>
                                            
                            </div> <!--news box-->
                          </div>
                          <div id="tab-news-2" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <div class="nb-content">
                                <div class="news-list ">
                                  @foreach($sub_degrees as $row)
                                  <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="#">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        <span class="wrap_download" style="font-size: 18px;float:right;color: #29292996;">{{__('general.download')}}: <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">EN</a> / <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">KH</a></span>
                                        
                                      </div> <!--meta-->
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('law-and-regulation',['locale'=>$locale,'slug'=>'sub-degree'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div> 
                                </div> <!--news list-->
                              </div>
                                            
                            </div> <!--news box-->
                          </div>
                          <div id="tab-news-3" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <div class="nb-content">
                                <div class="news-list ">
                                  @foreach($text_announcements as $row)
                                  <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="#">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        <span class="wrap_download" style="font-size: 18px;float:right;color: #29292996;">{{__('general.download')}}: <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">EN</a> / <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">KH</a></span>
                                        
                                      </div> <!--meta-->
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('law-and-regulation',['locale'=>$locale,'slug'=>'announcement'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div> 
                                </div> <!--news list-->
                              </div>
                                            
                            </div> <!--news box-->
                          </div>
                          <div id="tab-news-4" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <div class="nb-content">
                                <div class="news-list ">
                                  @foreach($decisions as $row)
                                  <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="#">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        <span class="wrap_download" style="font-size: 18px;float:right;color: #29292996;">{{__('general.download')}}: <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">EN</a> / <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">KH</a></span>
                                        
                                      </div> <!--meta-->
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('law-and-regulation',['locale'=>$locale,'slug'=>'decision'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div> 
                                </div> <!--news list-->
                              </div>
                                            
                            </div> <!--news box-->
                          </div>
                          <div id="tab-news-5" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="news-box  base-box">
                              <div class="nb-content">
                                <div class="news-list ">
                                  @foreach($onthers as $row)
                                  <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                    <div class="news-summary has-feature-image">
                                      <h3><a href="#">{{$row->title}}</a></h3>
                                      <div class="mom-post-meta nb-item-meta">
                                        <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                        <span class="wrap_download" style="font-size: 18px;float:right;color: #29292996;">{{__('general.download')}}: <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">EN</a> / <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">KH</a></span>
                                        
                                      </div> <!--meta-->
                                    </div>
                                  </article>
                                  @endforeach
                                  <div class="more">  
                                    <a href="{{route('law-and-regulation',['locale'=>$locale,'slug'=>'other'])}}"><span>{{__('general.view-more')}}</span></a>
                                  </div> 
                                </div> <!--news list-->
                              </div>
                                            
                            </div> <!--news box-->
                          </div>
                        </div>

                      </div>
                    </div> 
                  </div> 
                </div> 
              </div> 
          </div>

        </div> <!--main container-->            
        
        <!-- Sidebar -->

        @include('frontend.sidebar.related_post')
        <!-- EndSidebar -->

    </div> <!--content boxed wrapper-->
     
  

@endsection