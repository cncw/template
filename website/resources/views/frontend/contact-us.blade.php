@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-contact-us', 'current-menu-item')

@section ('appbottomjs')
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBbz45_RGsB8xrJtKSgdnL8jJTw0dX-nNw"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
    $(document).ready(function() {
      $("#contact-form").submit(function(event){
        name = $("#name").val();
        organization = $("#organization").val();
        position = $("#position").val();
        phone = $("#phone").val();
        subject = $("#subject").val();
        email = $("#email").val();

        message = $("#message").val();
        g =$('#g-recaptcha-response').val();
        
        if(name != ""){
          if(organization != ""){
            if(position != ""){
              if(phone != ""){
          if(subject != ""){
            if(email != ""){
              if(isEmail(email)){
                if(message != ""){
                  if(g != ""){
                    //alert('Go!');
                  }else{
                    error(event, "g-recaptcha-response", '{{ __('general.errorrecaptcha') }}');
                  }
         
                }else{
                  error(event, "message", '{{ __('general.errormessage') }}');
                }
              }else{
                error(event, "email", '{{ __('general.incorrectemail') }}');
              }
            }else{
              error(event, "email", '{{ __('general.erroremail') }}.');
            }
          }else{
            error(event, "subject", '{{ __('general.errorsubject') }}');
          }
          }else{
          error(event, "phone", '{{ __('general.errorphone') }}');
        }
          }else{
          error(event, "position", '{{ __('general.errorposition') }}');
        }
          }else{
          error(event, "organization", '{{ __('general.errororganization') }}');
        }
        }else{
          error(event, "name", '{{ __('general.errorname') }}');
        }
      })

      @if(Session::has('msg'))
        toastr.success("{{ __('general.contact-successful-sent') }}");
      @endif
      @if (count($errors) > 0)
        toastr.warning("{{ __('general.sorry') }}");
      @endif

    });
    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
    function error(event, obj, msg){
      event.preventDefault();
      toastr.error(msg);
      $("#"+obj).focus();
    }
   
  </script>
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
      <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post --> 
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div class="vc_row wpb_row vc_row-fluid"  >
                  <div class="vc_col-sm-12 wpb_column vc_column_container ">
                    <div class="wpb_wrapper">      
                      <div class="wpb_tabs main_tabs base-box wpb_content_element" data-interval="0">
                        <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
                          <ul class="tabs ui-tabs-nav vc_clearfix">
                            <li><a href="#tab-news">{{__('general.contact-us')}}</a></li>
                          </ul>
                          <div class="tabs-content-wrap">         
                            <div id="tab-news" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                              <div class="news-box  base-box">
                                <div class="nb-content">
                                  <div class="news-list ">
                                    <div class="sectin-cnt">
                                      <div class="content-column col-md-5 col-sm-12 col-xs-12">
                                          <ul class="list font-i2">
                                              <li><a href="https://www.mpwt.gov.kh" class="clearfix"><i class="fa fa-globe"></i>&nbsp;  {!!$contact_website->content!!} </a></li>
                                              <li><a href="#" class="clearfix"><i class="fa fa-phone"></i>&nbsp;  {!!$contact_phone->content!!} </a></li>
                                              <li><a href="#" class="clearfix"><i class="fa fa-envelope"></i>&nbsp;  {!!$contact_email->content!!} </a></li>
                                              <li><a href="#" class="clearfix"><i class="fa fa-map-marker"></i>&nbsp;  {!!$contact_address->content!!} </a></li>
                                          </ul>
                                      </div>
                                      <div class="image-column col-md-7 col-sm-12 col-xs-12">
                                       <iframe
                                          width="100%"
                                          height="200"
                                          frameborder="0" style="border:0"
                                          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCOVmFlwfcjVJE1mgzI69HIOnIwLYEW1OM
                                            &q=Ministry+of+Women's+Affairs" allowfullscreen>
                                        </iframe>
                                      </div>

                                      <div class="col-md-12">
                                        <hr>
                                        <div class="">
                                        <h4>{{__('general.send-message')}}</h4>
                                             @if (count($errors) > 0)
                                                <div class="form-group row">
                                                  <label for="message" class="col-sm-2 col-form-label"> </label>
                                                  <div class="col-sm-10">
                                                    @foreach ($errors->all() as $error)
                                                    <div class="alert-group">
                                                      <div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> {{ $error }}!</div>
                                                    </div>
                                                    @endforeach
                                                  </div>
                                                </div>
                                              @endif
                                             <div class="contact-form">
                                                <form method="POST" id="contact-form" action="{{ route('submit-contact', ['locale'=>$locale]) }}"  novalidate="novalidate">
                                                    {{ csrf_field() }}
                                                    {{ method_field('PUT') }}
                                                    <div class="row clearfix">
                                                        
                                                        <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                                            <input type="text" name="name" id="name" value="" placeholder="{{ __('general.your-name') }}" required="">
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <input type="text" name="organization" id="organization" value="" placeholder="{{ __('general.your-organization') }}" required="">
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <input type="text" name="position" id="position" value="" placeholder="{{ __('general.position') }}" required="">
                                                        </div>

                                                         <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <input type="text" name="phone" id="phone" value="" placeholder="{{ __('general.your-phone') }}" required="">
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <input type="email" name="email" id="email" value="" placeholder="{{ __('general.your-email') }}" required="">
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <input type="text" name="subject" id="subject" value="" placeholder="{{ __('general.your-subject') }}" required="">
                                                        </div>
                                                       
                                                        
                                                        <div class="column col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <textarea name="message" id="message" placeholder="{{ __('general.message') }} . . ."></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="column col-md-6 col-sm-12 col-xs-12 text-right">
                                                           <div class="g-recaptcha" data-sitekey="6LfBSx8UAAAAAA29jztOsyr3Rb8CcILAkB5yZ4Zk"></div>
                                                        </div>
                                                        <div class="column col-md-6 col-sm-12 col-xs-12 text-right">
                                                            <br />
                                                            <div class="form-group">
                                                                <button type="submit" class=" btn default-button blue-btn"><i class="fa fa-send"></i> {{ __('general.send-message') }}</button>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </form>
                                            </div>
                                      </div>
                                      </div>
                                    </div>
                                  </div> <!--news list-->
                                </div>
                                              
                              </div> <!--news box-->
                            </div> 
                          </div>
                        </div>
                      </div> 
                    </div> 
                  </div> 
                </div> 
            </div>
        </div> 
        <!--main container-->            
        <!-- Sidebar -->
        @include('frontend.sidebar.related_post')
        <!-- EndSidebar -->
      </div>           

    </div> <!--content boxed wrapper-->
     


@endsection