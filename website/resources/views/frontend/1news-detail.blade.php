@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-news-and-activities', 'current-menu-item')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
       <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post --> 
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div id="tab-prokas" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
               <div class="news-box  base-box">
                <header class="nb-header" >
                  <h2 class="nb-title" style=";">
                    <span style="line-height: 35px;" >@if($data) {{$data->title}} @endif</span></h2><br>
                  <hr style="margin:0px;">
                 @if($data) <p style="color:grey;"> {{Carbon\Carbon::parse($data->created_at)->format('F') }}-{{ Carbon\Carbon::parse($data->created_at)->format('d') }}-{{ Carbon\Carbon::parse($data->created_at)->format('Y') }}</p> @endif
                </header> <!--nb header-->
                <div class="nb-content">
                  <div class="news-list ">
                    <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                      <div class="col-md-12 no_padd">
                        <img src="@if($data) {{ asset ($data->image)}} @endif" class="full-width" />
                        <hr style="">
                        <P>@if($data) {!!$data->content!!} @endif</P>
                      </div>

                      </div>
                    </article>
                  </div> <!--news list-->
                </div>
              </div> 
            </div> 
          </div>
        </div> <!--main container--> 
        <!--main container-->            
        <!-- Sidebar -->
        @include('frontend.sidebar.related_post')
        <!-- EndSidebar -->
      </div>          
    </div>
     


@endsection