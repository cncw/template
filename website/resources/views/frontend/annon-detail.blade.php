@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-home', 'active')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
      <!--Running Text -->
      <div class="nav-shaddow"></div>
      <div style="margin-top:-17px; margin-bottom:20px;"></div>            
       <div class="inner">                        
        <div class="breaking-news">
          <div class="the_ticker" >
            <div class="bn-title"><span>ពត័មានថ្មីៗ</span></div>
            <div class="news-ticker " data-timeout="5000">
              <ul>
                  <li><a href="#"><i class="fa fa-angle-double-right"></i> នៅក្នុងឱកាសបុណ្យចូលឆ្នាំថ្មី ឆ្នាំច សំរឹទិ្ធស័ក ព.ស ២៥៦២ ខិតជិតចូលមកដល់ គណ:ប្រតិភូក្រសួងកិច្ចការនារី និងមន្ទីរកិច្ចការនារីទាំង២៥ខេត្តក្រុង ដឹកដោយលោកជំទាវវេជ្ជ. អ៊ឹង កន្ថាផាវី រដ្ឋមន្ត្រីក្រសួងកិច្ចការនារី បានចុះសួរសុខទុក្ខ និងសំណេះសំណាលជាមួយបងប្អូនកងទ័ពនៃវរសេនាតូចលេខ២</a></li>
                  <li><a href="#"><i class="fa fa-angle-double-right"></i> កាលពីថ្ងៃអង្គារ ១០រោចខែចែត្រនព្វស័កព.ស ២៥៦១ ត្រូវនឹងថ្ងៃទី១០ ខែមេសា គ.ស២០១៨ ក្រុមប្រឹក្សាជាតិកម្ពុជាដើម្បីស្រ្តីបានរៀបចំវគ្គបណ្តុះបណ្តាលស្តីពីការអនុវត្ត អនុសញ្ញា “ការលុបបំបាត់រាល់ទម្រង់នៃការរើសអើងប្រឆាំងនឹងនារីភេទ”</a></li>
                  <li></i><a href="#"><i class="fa fa-angle-double-right"></i>កាលពីថ្ងៃទី០៣ដល់ ថ្ងៃទី០៥​ ខែមេសា ឆ្នាំ២០១៨ ក្រុមប្រឹក្សាជាតិកម្ពុជា​ដើម្បីស្រ្តីនៃ​ក្រសួងកិច្ចការនារី បានចុះតាមដានវាយតម្លៃ​ ការអនុវត្តអនុសញ្ញាស្ដីពីការលុបបំបាត់រាល់ទម្រង់នៃការរើសអើងប្រឆាំងនារីភេទ</a></li>
                  <li></i><a href="#"><i class="fa fa-angle-double-right"></i>កិច្ចសហការ រវាងមន្ទីរកិច្ចការនារី និងមន្ទីរអប់រំយុវជន និងកីឡាខេត្តកោះកុង នាយកដ្ឋានព័ត៌មាននៃក្រសួងកិច្ចការនារីបានរៀបចំវេទិកានារីរតនៈ ក្រោមប្រធានបទ “យុវជនរួមគ្នាកាត់បន្ថយអំពើហិង្សា”</a></li>
              </ul>
            </div> <!--news ticker-->
          </div>
          <span class="current_time">    GMT+7 10:52      </span>
        </div> <!--breaking news-->
      </div> 
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div id="tab-prokas" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
               <div class="news-box  base-box">
                <header class="nb-header" >
                  <h2 class="nb-title" style=";">
                    <span >កិច្ចប្រជុំដើម្បីត្រៀមរៀបចំអនុវត្តផែនការយុទ្ធសាស្រ្តជាតិស្តីពីអត្តសញ្ញាណកម្ម</span></h2><br>
                  <hr style="margin:0px;">
                  <p style="color:grey;">20-may-2018</p>
                </header> <!--nb header-->
                <div class="nb-content">
                  <div class="news-list ">
                    <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                      <div class="col-md-12 no_padd">
                        <img src="{{ asset ('public/frontend/images/alert/alert5.jpg')}}" class="full-width" />
                        <hr style="">
                        <P>នាថ្ងៃទី០១ ខែកុម្ភៈ ឆ្នាំ២០១៧ អគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម ដឹកនាំដោយឯកឧត្តម នាយឧត្តមសេនីយ៍ ម៉ៅ ច័ន្ទតារា អគ្គនាយក នៃអគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម បានចូលរ...</P>
                        <P>នាថ្ងៃទី០១ ខែកុម្ភៈ ឆ្នាំ២០១៧ អគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម ដឹកនាំដោយឯកឧត្តម នាយឧត្តមសេនីយ៍ ម៉ៅ ច័ន្ទតារា អគ្គនាយក នៃអគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម បានចូលរ...</P>
                        <P>នាថ្ងៃទី០១ ខែកុម្ភៈ ឆ្នាំ២០១៧ អគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម ដឹកនាំដោយឯកឧត្តម នាយឧត្តមសេនីយ៍ ម៉ៅ ច័ន្ទតារា អគ្គនាយក នៃអគ្គនាយកដ្ឋានអត្តសញ្ញាណកម្ម បានចូលរ...          <a href="#" class="read-more-link">អានបន្ថែម <i class="fa fa-angle-double-right"></i></a></P>
                      </div>

                      </div>
                    </article>
                  </div> <!--news list-->
                </div>
              </div> 
            </div> 
          </div>
        </div> <!--main container--> 
        <!--main container-->            
        <!-- Sidebar -->
        @include('frontend.sidebar.related_post')
        <!-- EndSidebar -->
      </div>          
    </div>
     


@endsection