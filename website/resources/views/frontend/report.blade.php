@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-report', 'current-menu-item')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
     <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post -->
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div class="vc_row wpb_row vc_row-fluid"  >
                  <div class="vc_col-sm-12 wpb_column vc_column_container ">
                    <div class="wpb_wrapper">      
                      <div class="wpb_tabs main_tabs base-box wpb_content_element" data-interval="0">
                        <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
                          <ul class="tabs ui-tabs-nav vc_clearfix">
                            <li><a href="#tab-news">{{__('general.report')}}</a></li>
                          </ul>
                          <div class="tabs-content-wrap">         
                            <div id="tab-news" class="tab-content ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                              <div class="news-box  base-box">
                                <div class="nb-content">
                                  <div class="news-list ">
                                    @foreach($data as $row)
                                    <article class="nl-item post-4212 post type-post status-publish format-standard has-post-thumbnail category-news-01" itemscope itemtype="http://schema.org/Article">
                                      <div class="news-summary has-feature-image">
                                        <h3><a href="#">{{$row->title}}</a></h3>
                                        <div class="mom-post-meta nb-item-meta">
                                          <span datetime="2017-02-02T11:14:27+00:00" class="entry-date">{{ Carbon\Carbon::parse($row->created_at)->format('F') }} {{ Carbon\Carbon::parse($row->created_at)->format('d') }}, {{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span>
                                          <span class="wrap_download" style="font-size: 18px;float:right;color: #29292996;">{{__('general.download')}}: <a target="_blank" href="{{asset($row->en_pdf)}}" class="my_download">EN</a> / <a target="_blank" href="{{asset($row->kh_pdf)}}" class="my_download">KH</a></span>
                                          
                                        </div> <!--meta-->
                                      </div>
                                    </article>
                                    @endforeach

                                    <div class="pad10">
                                      <div class="text-center">  
                                          {{ $data->links('vendor.pagination.frontend-html') }}
                                      </div> 
                                    </div>


                                  </div> <!--news list-->
                                </div>
                                              
                              </div> <!--news box-->
                            </div>
                          
                          </div>
                        </div>
                      </div> 
                    </div> 
                  </div> 
                </div> 
            </div>
        </div> 
        <!--main container-->            
        <!-- Sidebar -->
        @include('frontend.sidebar.related_post')
        <!-- EndSidebar -->
      </div>          
    </div>
     


@endsection