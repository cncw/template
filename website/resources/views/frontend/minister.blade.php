@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-home', 'current-menu-item')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
      <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post -->
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div class="widget1 widget_text">
                <div class="widget-head"><h3 class="widget-title">
                  <span>{{__('general.message-from-president')}}</span></h3>
                </div>
                <div class="textwidget1">
                  <div class="row">
                      <div class="col-sm-6">
                        <img src="{{ asset ('public/frontend/images/ministers.jpg')}}">

                      </div>
                      <div class="col-sm-6">
                        @if($bio) {!!$bio->content!!} @endif
                        <!-- <h4 style=" margin-left: 4px;font-family: " proxima="" nova",="" sans-serif;="" color:="" rgb(0,="" 0,="" 0);"="">ឯកឧត្តមទេសរដ្ឋមន្ត្រី ស៊ុន ចាន់ថុល</h4>
                            <table>
                               <tbody>
                                  <tr>
                                     <td style="width: 187px;">ថ្ងៃខែឆ្នាំកំណើត</td>
                                     <td>: ឆ្នាំ 1956</td>
                                  </tr>
                                  <tr>
                                     <td>ទីកន្លែងកំណើត</td>
                                     <td>: ភ្នំពេញ កម្ពុជា</td>
                                  </tr>
                                  <tr>
                                     <td>សញ្ជាតិ</td>
                                     <td>: ខ្មែរ</td>
                                  </tr>
                                  <tr>
                                     <td>ភេទ</td>
                                     <td>: ប្រុស</td>
                                  </tr>
                                  <tr>
                                     <td>ស្ថានភាពគ្រួសារ</td>
                                     <td>: រៀបការរួចហើយមានកូនបីនាក់</td>
                                  </tr>
                                  <tr>
                                     <td>អាស័យដ្ឋាន</td>
                                     <td>: ផ្ទះលេខ 18 ផ្លូវលេខ 302 បឹងកេងកង 1 ខណ្ឌចំការមនរាជធានីភ្នំពេញ ប្រទេសកម្ពុជា</td>
                                  </tr>
                               </tbody>
                            </table>              -->       
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12 wrap-minis font-i2 default_text">
                        @if($spech) {!!$spech->content!!} @endif
                      <!-- <br>
                      <h4>ការអប់រំ</h4>
                      <ul style="margin-left: 0px; padding: 15px; border: none; outline-style: none; outline-width: initial; color: rgb(128, 128, 128); font-family: Hanuman, serif; font-size: 15px;"><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• បរិញ្ញាប័ត្រវិទ្យាសាស្រ្តផ្នែកគ្រប់គ្រងពា ណិជ្ជកម្មពីសា កលវិទ្យាល័យអាមេ រិក - ឆ្នាំ១៩៧៨</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• កម្មវិធីគ្រប់គ្រងជាន់ខ្ពស់ ពីសាកលវិទ្យាល័យ Whartonរដ្ឋ Pennsylvania - ឆ្នាំ១៩៩៧</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• អនុបណ្ឌិតផ្នែករដ្ឋបាលសាធារណៈពីសាកលវិទ្យា Harvard - ឆ្នាំ១៩៩៩</li></ul>                      
                      <h4>ប្រវត្តិទាក់ទងនឹងវិជ្ជជីវៈ</h4>
                      <ul style="margin-left: 0px; padding: 15px; border: none; outline-style: none; outline-width: initial; list-style-type: none; color: rgb(128, 128, 128); font-family: Hanuman, serif; font-size: 15px;"><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• បានបម្រើការឲ្យក្រុមហ៊ុន General Electric រយៈពេល ១៦ ឆ្នាំនៅក្នុងមុខតំណែងប្រតិបត្តិជាន់ខ្ពស់ជាច្រើន</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ការគ្រប់គ្រងទូទៅនិងហិរញ្ញវត្ថុ (នៅសហរដ្ឋអាមេ រិក បារាំង និងថៃ)</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• សវនករក្រុមហ៊ុន General Electric</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• បង្កើតក្រុមហ៊ុនពាណិជ្ជកម្ម និងវិនិយោគកម្ពុជា - ១៩៩៩។</li></ul>
                      <h4>ការទទួលខុសត្រូវផ្នែកនយោបាយ</h4>
                      <ul style="margin-left: 0px; padding: 15px; border: none; outline-style: none; outline-width: initial; list-style-type: none; color: rgb(128, 128, 128); font-family: Hanuman, serif; font-size: 15px;"><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">•ចាប់ពីឆ្នាំ១៩៩៤&nbsp;បានធ្វើមាតុនិវត្តន៍មកកាន់ប្រទេសកម្ពុជាវិ ញក្នុងឆ្នាំ១៩៩៤ ដើម្បីជួយរៀបចំបង្កើតក្រុមប្រឹក្សាអភិវឌ្ឍន៍កម្ពុជា និងបង្កើត ច្បាប់វិនិយោគ<br></li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ចាប់ពីឆ្នាំ១៩៩៤-១៩៩៧ បានបម្រើការជារដ្ឋលេខា ធិការក្រសួងសេដ្ឋកិច្ចនិងហិរញ្ញវត្ថុ និងអគ្គលេខា ធិការក្រុមប្រឹក្សាអភិវឌ្ឍន៍កម្ពុជាចា ប់ពីឆ្នាំ</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ចាប់ពីឆ្នាំ១៩៩៩-២០០៣ បានបម្រើការជាទីប្រឹក្សាសេដ្ឋកិច្ច និងហិរញ្ញវត្ថុ ឲ្យប្រធា នសភាជាតិ</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ចាប់ពីឆ្នាំ២០០៤ ដល់ ឆ្នាំ២០០៨ បានបម្រើការជារដ្ឋមន្រ្តីក្រសួងសាធារណៈការ និងដឹកជញ្ជូន</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ចាប់ពី ឆ្នាំ២០១៣ ដល់ឆ្នាំ២០១៣ ត្រូវបានតែងតាំងជាទេសរដ្ឋមន្រ្តី និងអនុប្រធាន CDC ឲ្យប្រធា នសភាជាតិ</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ចាប់ពី ២០១៣-២០១៦ ត្រូវបានតែងតាំងជាទេសរដ្ឋមន្រ្តីក្រសួងពាណិជ្ជកម្ម និងជា អនុ ប្រធា នក្រុមប្រឹក្សាអភិវឌ្ឍន៍កម្ពុជា</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ចាប់ពីខែមេ សា ឆ្នាំ២០១៦ រដ្ឋមន្ត្រីក្រសួងសាធារណៈការ និងដឹកជញ្ជូន</li></ul>
                      <h4>សកម្មភាពវិជ្ជាជីវៈផ្សេងទៀត</h4>
                      <ul style="margin-left: 0px; padding: 15px; border: none; outline-style: none; outline-width: initial; list-style-type: none; color: rgb(128, 128, 128); font-family: Hanuman, serif; font-size: 15px;"><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• បានជាប់ឆ្នោតជាអ្នកដឹកនាំពិភពលោកថ្ងៃស្អែកនៃវេទិកាសេដ្ឋកិច្ចពិភពលោកក្នុងឆ្នាំ១៩៩៥</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• សមាជិកក្រុមប្រឹក្សាប្រតិបត្តិនៃ Wharton Asianតាំងពីឆ្នាំ ២០០២</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ប្រធា នក្រុមប្រឹក្សាប្រតិបត្តិនៃ Wharton Asian ចាប់ពីឆ្នាំ២០០៩ ដល់ ឆ្នាំ២០១៣</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ប្រធា នសហព័ន្ធកីឡាហែលទឹកកម្ពុជា</li></ul>                      
                      <h4>បញ្ហាប្រឈមក្នុងនាមជារដ្ឋមន្រ្តី</h4>
                      <ul style="margin-left: 0px; padding: 15px; border: none; outline-style: none; outline-width: initial; list-style-type: none; color: rgb(128, 128, 128); font-family: Hanuman, serif; font-size: 15px;"><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ធ្វើកំណែទម្រង់ និងលុបបំបាត់អំពើពុករលួយនៅក្នុងក្រសួង</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ធ្វើឲ្យប្រសើរដល់ការដឹកជញ្ជូន និងហេដ្ឋរចនាសម្ព័ន្ធ និង ទាក់ទាញការវិនិយោគផ្ទាល់ពីបរទេសលើវិស័យនេះ</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ធ្វើកំណែទម្រង់ឲ្យមានប្រសិទ្ធភាពក្នុងកា រចេញប័ណ្ណបើកបរថ្មីព្រមទាំងការផ្លាស់ប្តូរយកប័ណ្ណបើកបរថ្មី</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• ធ្វើឲ្យប្រសើរឡើងដល់ដំណើរការចុះបញ្ជីយា នយន្ត</li><li style="border: none; outline-style: none; outline-width: initial; list-style: none;">• បង្កើនសុវត្ថិភាពចរាចរណ៍តាមរយៈការធ្វើឲ្យប្រសើរឡើងដល់ ចំនួននៃការត្រួតពិនិត្យយានយន្ត</li></ul>                    </div> -->
                  </div>
                </div>
            </div>
          </div>
        </div> <!--main container-->            
        
        <!-- Sidebar -->
        
        @include('frontend.sidebar.related_post')
        
        <!-- EndSidebar -->

    </div> <!--content boxed wrapper-->
     


@endsection