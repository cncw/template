<div class="nav-shaddow"></div>
      <div style="margin-top:-17px; margin-bottom:20px;"></div>            
       <div class="inner">                        
        <div class="breaking-news">
          <div class="the_ticker" >
            <div class="bn-title"><span>{{__('general.lastest-news')}}</span></div>
            <div class="news-ticker " data-timeout="5000">
              <ul>
                 @php($lastest_news = $defaultData['lastest_news'])
                 @foreach($lastest_news as $row)
                  <li><a href="#"><i class="fa fa-angle-double-right"></i> {{$row->title}} : {{$row->description}}</a></li>
                  @endforeach
                  <!-- 
                  <li><a href="#"><i class="fa fa-angle-double-right"></i> កាលពីថ្ងៃអង្គារ ១០រោចខែចែត្រនព្វស័កព.ស ២៥៦១ ត្រូវនឹងថ្ងៃទី១០ ខែមេសា គ.ស២០១៨ ក្រុមប្រឹក្សាជាតិកម្ពុជាដើម្បីស្រ្តីបានរៀបចំវគ្គបណ្តុះបណ្តាលស្តីពីការអនុវត្ត អនុសញ្ញា “ការលុបបំបាត់រាល់ទម្រង់នៃការរើសអើងប្រឆាំងនឹងនារីភេទ”</a></li>
                  <li></i><a href="#"><i class="fa fa-angle-double-right"></i>កាលពីថ្ងៃទី០៣ដល់ ថ្ងៃទី០៥​ ខែមេសា ឆ្នាំ២០១៨ ក្រុមប្រឹក្សាជាតិកម្ពុជា​ដើម្បីស្រ្តីនៃ​ក្រសួងកិច្ចការនារី បានចុះតាមដានវាយតម្លៃ​ ការអនុវត្តអនុសញ្ញាស្ដីពីការលុបបំបាត់រាល់ទម្រង់នៃការរើសអើងប្រឆាំងនារីភេទ</a></li>
                  <li></i><a href="#"><i class="fa fa-angle-double-right"></i>កិច្ចសហការ រវាងមន្ទីរកិច្ចការនារី និងមន្ទីរអប់រំយុវជន និងកីឡាខេត្តកោះកុង នាយកដ្ឋានព័ត៌មាននៃក្រសួងកិច្ចការនារីបានរៀបចំវេទិកានារីរតនៈ ក្រោមប្រធានបទ “យុវជនរួមគ្នាកាត់បន្ថយអំពើហិង្សា”</a></li> -->
              </ul>
            </div> <!--news ticker-->
          </div>
          <span class="current_time">    GMT+7 10:52      </span>
        </div> <!--breaking news-->
      </div> 