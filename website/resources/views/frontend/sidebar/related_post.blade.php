<div class="sidebar main-sidebar">
          <!-- <div class="widget widget_text">
            <div class="widget-head"><h3 class="widget-title">
              <span>រដ្ដមន្ត្រី</span></h3>
            </div>
            <div class="textwidget">
              <a href=""><img src="{{ asset ('public/frontend/images/minister1.jpg')}}"></a>
            </div>
          </div> -->

          <div class="widget widget_text">
            <div class="widget-head"><h3 class="widget-title">
              <span>{{__('general.minister')}}</span></h3>
            </div>
            <div class="textwidget">
              <a href="{{route('minister',$locale)}}"><img src="{{ asset ('public/frontend/images/director.jpg')}}"></a>
            </div>
          </div>

          <div class="widget momizat-posts">
            <div class="widget-head">
              <h3 class="widget-title"><span>{{__('general.video')}}</span></h3>
            </div>
            @foreach($defaultData['videos'] as $row)
              <div class="textwidget">
                <iframe  src="https://www.youtube.com/embed/{{$row->url}}"></iframe>
                <h4>{{$row->title}}</h4>
              </div>
            @endforeach
            <!-- <div class="textwidget">
              <iframe  src="https://www.youtube.com/embed/LqcKuOix4-k"></iframe>
              <h4>ABC</h4>
            </div> -->

          </div>

          <!-- <div class="widget widget_wp_statsmechanic"><div class="widget-head"><h3 class="widget-title"><span>ចំនួនអ្នកទស្សនា</span></h3></div>  <link rel="stylesheet" type="text/css" href="http://news.gdi.gov.kh/wp-content/plugins/mechanic-visitor-counter/styles/css/default.css" /><div id="mvcwid" style="font-size:2; text-align:Center;color:;">
            
            <div id="mvctable">
                <table width="100%">
                  <tr><td style="font-size:2; text-align:Center;color:;"><img src="http://news.gdi.gov.kh/wp-content/plugins/mechanic-visitor-counter/counter/mvcvisit.png"> Visit Today : 26</td></tr>
                  <tr><td style="font-size:2; text-align:Center;color:;"><img src="http://news.gdi.gov.kh/wp-content/plugins/mechanic-visitor-counter/counter/mvcmonth.png"> This Month : 844</td></tr>
                  <tr><td style="font-size:2; text-align:Center;color:;"><img src="http://news.gdi.gov.kh/wp-content/plugins/mechanic-visitor-counter/counter/mvcyear.png"> This Year : 3853</td></tr>
                  <tr><td style="font-size:2; text-align:Center;color:;"><img src="http://news.gdi.gov.kh/wp-content/plugins/mechanic-visitor-counter/counter/mvctotal.png"> Total Visit : 47816</td></tr>
                  <tr><td style="font-size:2; text-align:Center;color:;"><img src="http://news.gdi.gov.kh/wp-content/plugins/mechanic-visitor-counter/counter/mvconline.png"> Who's Online : 1</td></tr>
                </table>
            </div>
          </div>
              </div> -->
      </div>
                        