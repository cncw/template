@extends('frontend/layouts.master')

@section('title', 'About Us | Department of Good Govener')
@section('active-about-us', 'current-menu-item')

@section ('appbottomjs')
@endsection
@section ('home')
@endsection

@section ('content')

      <div class="boxed-wrap clearfix">
    <div class="boxed-content-wrapper clearfix">             
      
      <!--Slide Post -->
      @include('frontend.sidebar.slide_post')
      <!--End Slide Post -->
      <div class="inner">
        <div class="main-container">
          <div class="main-col">
            <div class="widget1 widget_text">
                <div class="widget-head"><h3 class="widget-title">
                  <span>@if($data) {{$data->name}} @endif</span></h3>
                </div>
                <div class="textwidget1">
                  <p>@if($data) {!!$data->content!!} @endif</p>
                </div>
            </div>
          </div>
        </div> <!--main container-->            
        
        <!-- Sidebar -->
        
        @include('frontend.sidebar.related_post')
        
        <!-- EndSidebar -->

    </div> <!--content boxed wrapper-->
     


@endsection