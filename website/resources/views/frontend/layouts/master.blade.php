<!DOCTYPE html>
<html lang="en">
  <head>
    <title>CNCW</title>
      <!-- Required meta tags -->
      <meta charset="UTF-8" />
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  <link rel="profile" href="http://gmpg.org/xfn/11">
	  <link rel="pingback" href="http://news.gdi.gov.kh/xmlrpc.php">
	  <meta property="og:image" content=""/>
	  <meta property="og:title" content="ទំព័រដើម"/>
	  <meta property="og:type" content="article"/>
	  <meta property="og:description" content=""/>
	  <meta property="og:url" content="http://news.gdi.gov.kh/"/>
	  <meta property="og:site_name" content="Department of Good Govener"/>
	  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  <link rel="icon" href="{{ asset ('public/frontend/images/fav.ico')}}" type="image/x-icon">


	  <link rel="pingback" href="http://news.gdi.gov.kh/xmlrpc.php" />
	  <link rel="dns-prefetch" href="//fonts.googleapis.com" />
	  <link rel="dns-prefetch" href="//s.w.org" />
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


      
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/style1.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/style2.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/entypo" type="text/css"/>
  <style id="rs-plugin-settings-inline-css" type="text/css">
  .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
  </style>
  <link rel="stylesheet" id="tp-hanuman-css"  href="http://fonts.googleapis.com/css?family=Hanuman&#038;ver=4.7.2" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/style3.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/style4.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/style5.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/style6.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/plugins.css')}}" type="text/css" media="all" />
  <link rel="stylesheet" id="contact-form-7-css"  href="{{ asset ('public/frontend/css/camcyber.css')}}" type="text/css" media="all" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" id="redux-google-fonts-css"  href="http://fonts.googleapis.com/css?family=Battambang%3A400%2C700%7CHanuman%3A400%2C700&#038;subset=khmer&#038;ver=1455691178" type="text/css" media="all" />  
  <link rel="shortcut icon" href="http://news.gdi.gov.kh/wp-content/uploads/2015/10/favicon.png" />   
  
  <style type="text/css" title="dynamic-css" class="options-output">
      .mom_main_font,.topbar,#navigation .main-menu,.breaking-news,.breaking-news .bn-title,.feature-slider li .slide-caption h2,.news-box .nb-header .nb-title,a.show-more,.widget .widget-title,.widget .mom-socials-counter ul li,.main_tabs .tabs a,.mom-login-widget,.mom-login-widget input,.mom-newsletter h4,.mom-newsletter input,.mpsw-slider .slide-caption,.tagcloud a,button,input,select,textarea,.weather-widget,.weather-widget h3,.nb-inner-wrap .search-results-title,.show_all_results,.mom-social-share .ss-icon span.count,.mom-timeline,.mom-reveiw-system .review-header h2,.mom-reveiw-system .review-summary h3,.mom-reveiw-system .user-rate h3,.mom-reveiw-system .review-summary .review-score,.mom-reveiw-system .mom-bar,.mom-reveiw-system .review-footer,.mom-reveiw-system .stars-cr .cr,.mom-reveiw-system .review-circles .review-circle,.p-single .post-tags,.np-posts ul li .details .link,h2.single-title,.page-title,label,.portfolio-filter li,.pagination .main-title h1,.main-title h2,.main-title h3,.main-title h4,.main-title h5,.main-title h6,.mom-e3lan-empty,.user-star-rate .yr,.comment-wrap .commentnumber,.copyrights-area,.news-box .nb-footer a,#bbpress-forums li.bbp-header,.bbp-forum-title,div.bbp-template-notice,div.indicator-hint,#bbpress-forums fieldset.bbp-form legend,.bbp-s-title,#bbpress-forums .bbp-admin-links a,#bbpress-forums #bbp-user-wrapper h2.entry-title,.mom_breadcrumb,.single-author-box .articles-count,.not-found-wrap,.not-found-wrap h1,.gallery-post-slider.feature-slider li .slide-caption.fs-caption-alt p,.chat-author,.accordion .acc_title,.acch_numbers,.logo span,.device-menu-holder,#navigation .device-menu,.widget li .cat_num, .wp-caption-text, .mom_quote, div.progress_bar span, .widget_display_stats dl, .feature-slider .fs-nav.numbers a{font-family:Battambang;font-weight:normal;font-style:normal;}.button, .scrolling-box .sb-item h3, .widget ul li, .older-articles ul li, .copyrights-text, #comments .single-comment .comment-content .comment-reply-link, #comments .single-comment .comment-content .comment-edit-link, #navigation .main-menu > li .cats-mega-wrap .subcat li .subcat-title, .widget ul.twiter-list, #bbpress-forums ul.bbp-replies .bbp-reply-content .bbp-author-name, h1, h2, h3, h4, h5, h6{font-family:Hanuman;font-weight:normal;font-style:normal;}body{font-family:Hanuman;font-weight:400;font-style:normal;color:#8e8e8e;}.layout-boxed:not(.layout-boxed-content) .boxed-wrap, .layout-boxed-content .boxed-content-wrapper{background-color:transparent;}a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover,
      .news-ticker li a:hover, .mom-post-meta a:hover, .news-box .older-articles ul li a:hover,
      .news-box .nb1-older-articles ul li a:hover, .mom-login-widget .lw-user-info a:hover strong,
      .mpsw-slider ul.slides li .slide-caption:hover, .tagcloud a:hover, .mom-recent-comments .author_comment h4 span a:hover,
      .widget .twiter-list ul.twiter-buttons li a:hover, .copyrights-text a:hover, ul.main-menu li.mom_mega .mega_col_title  a:hover,
      #navigation .main-menu > li .cats-mega-wrap .subcat .mom-cat-latest li a:hover,
      #navigation .main-menu > li .cats-mega-wrap .subcat .mom-cat-latest .view_all_posts:hover,
      .base-box .read-more-link, .widget ul li a:hover, .main_tabs .tabs a.current, .button:hover,
      .weather-widget .next-days .day-summary .d-date span.dn, .np-posts ul li .details .link:hover,
      #comments .single-comment .comment-content .comment-reply-link:hover, #comments .single-comment .comment-content .comment-edit-link:hover,
      .single-author-box .articles-count, .star-rating, .blog-post .bp-head .bp-meta a:hover, ul.main-menu > li:not(.mom_mega) ul.sub-menu li a:hover,
      .not-found-wrap .ops, #bbpress-forums a,
      #navigation .main-menu > li:hover > a, #navigation .main-menu > li.current-menu-item > a, #navigation .main-menu > li.current-menu-ancestor > a,
      #navigation .main-menu > li:hover > a:before, #navigation .main-menu > li.current-menu-item > a:before, #navigation .main-menu > li.current-menu-ancestor > a:before,
      #navigation ul.device-menu li.dm-active > a, #navigation .device-menu li.dm-active > .responsive-caret, .widget li:hover .cat_num, .news-ticker li i, .mom_breadcrumb .sep, .scrollToTop:hover,
      ul.products li .mom_product_thumbnail .mom_woo_cart_bt .button:hover, .main_tabs .tabs li.active > a, .toggle_active .toggle_icon:before, #navigation .nav-button.active, .mom-main-color, .mom-main-color a,
      #buddypress div#item-header div#item-meta a, #buddypress div#subnav.item-list-tabs ul li.selected a, #buddypress div#subnav.item-list-tabs ul li.current a, #buddypress div.item-list-tabs ul li span, #buddypress div#object-nav.item-list-tabs ul li.selected a, #buddypress div#object-nav.item-list-tabs ul li.current a, .mom_bp_tabbed_widgets .main_tabs .tabs a.selected, #buddypress div.activity-meta a.button, .generic-button a, .top_banner a{color:#c4a938;}.mom-social-icons li a.vector_icon:hover, .owl-dot.active span, .feature-slider .fs-nav .selected, #navigation .nav-button.nav-cart span.numofitems{background:#c4a938;}#comments .single-comment .comment-content .comment-reply-link:hover, #comments .single-comment .comment-content .comment-edit-link:hover, .post.sticky{border-color:#c4a938;}h1, h2, h3, h4, h5, h6{color:#c4a938;}a{color:#1e73be;}a:hover{color:#25aaed;}a:active{color:#0d357a;}.navigation-inner,#navigation .nav-button, .nb-inner-wrap .search-results-title, .show_all_results, .nb-inner-wrap ul.s-results .s-img .post_format{background-color:#c4a938;}.main-menu > li:not(.current-menu-item):not(.current-menu-ancestor) > a, #navigation .nav-button, .nb-inner-wrap ul.s-results .s-details h4, .nb-inner-wrap .search-results-title, .show_all_results a,  .ajax_search_results .sw-not_found{font-weight:400;font-style:normal;color:#ffffff;}{color:#efefef;}ul.main-menu > li .cats-mega-wrap ul.sub-menu li.active a, #navigation .main-menu > li .cats-mega-wrap .subcat{background-color:#efefef;}{color:#c4a938;}.breaking-news .bn-title{background-color:#c4a938;}.breaking-news .bn-title:after{border-left-color:#c4a938;}body.rtl .breaking-news .bn-title:after{border-right-color:#c4a938;}.news-box .nb-header, .sidebar .widget .widget-head, .news-box .nb-header .nb-title a, .news-box .nb-header .nb-title span, .sidebar .widget .widget-title span{color:#c4a938;}
          
      .header > .inner, .header .logo {
      line-height: 155px;
      height: 155px;
      }
      .news-box .nb-item-meta a:hover {
          color: #b07412 !important;
      }
      .show_all_results a i, .search-wrap ul.s-results .s-img .post_format {
      color: #f7c53d;
      }
      .news-box .nb-header .nb-title, .sidebar .widget .widget-title {
          background-image: url(../assets/frontend/images/echopng.png);
      }
      .news-box .nb-header .nb-title, .sidebar .widget .widget-title{height: 4px;}
      #navigation .main-menu > li{border-width: 0px;}
      .current_time {display: none;}
      .the_ticker {margin-right: 0px;}
      .header > .inner, .header .logo{  width: 100%; text-align: center; margin-bottom: 43px;   }
      #navigation .main-menu > li:last-child{ border-right: 0px solid #dfdfdf !important; }
      .no-touch a img:hover{opacity: 10;}
      .main-menu > li > a{padding-left:20px;padding-right: 20px;}
      #navigation .nav-button a{color:#fff;}
      #navigation .nav-button:first-child{color:#fff;}
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
  </style>

  </head>

  <body class="home page-template-default page page-id-205 left-sidebar layout-boxed fade-imgs-in-appear navigation_highlight_ancestor time_in_twelve_format one-side-wide both-sidebars open_images_in_lightbox wpb-js-composer js-comp-ver-4.4.4 vc_responsive">
        <div class="boxed-wrap clearfix">
      		<div id="header-wrapper">
       			<header class="header">
		        	<div class="inner">
			          <div class="logo">
			            <h1>
			              <a href="">
			                <img src="{{ asset ('public/frontend/images/logo.jpg')}}" alt="GENERAL DEPARTMENT OF IDENTIFICATION" width="" height="" />
			              </a>
			            </h1>
			          </div>
			          <div class="header-right">
			            <div class="mom-e3lanat-wrap  ">
			              <div class="mom-e3lanat " style="">
			                <div class="mom-e3lanat-inner">
			                
			                </div>
			              </div>  <!--Mom ads-->
			            </div>
			          </div><!--header right-->                            
			          <div class="clear"></div>
		          </div>
		      </header>
      		</div><!--header wrap-->

	      <nav id="navigation" itemtype="#" itemscope="itemscope" role="navigation" class="dd-effect-slide ">
	        <div class="navigation-inner">
	          <div class="inner">
	            <ul id="menu-menu" class="main-menu mom_visibility_desktop">
                <li id="menu-item-1377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-205 current_page_item mom_default_menu_item menu-item-depth-0 @yield('active-home')"><a href="{{route('home',$locale)}}"><i class="fa fa-home"></i>{{__('general.home')}}</a></li>
	              
	              <li id="menu-item-862" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item mom_default_menu_item menu-item-depth-0 @yield('active-about-us')"><a href="#">{{__('general.about-us')}}</a>
	                <ul class="sub-menu ">
	                  <li id="menu-item-883" class="menu-item menu-item-type-post_type menu-item-object-page mom_default_menu_item menu-item-depth-1"><a href="{{route('about-us',$locale)}}?page=history">{{__('general.history')}}</a></li>
	                  <li id="menu-item-882" class="menu-item menu-item-type-post_type menu-item-object-page mom_default_menu_item menu-item-depth-1"><a href="{{route('about-us',$locale)}}?page=mission">{{__('general.mission')}}</a></li>
	                  <li id="menu-item-882" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item mom_default_menu_item active menu-item-depth-1"><a href="{{route('about-us',$locale)}}?page=vision">{{__('general.vision')}}</a>
	                </li>
	              </ul>
	              </li>
	              <li id="menu-item-1429" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item mom_default_menu_item menu-item-depth-0 @yield('active-law-and-regulation')"><a href="#">{{__('general.law-and-regulation')}}</a>
	                <ul class="sub-menu ">
                    @foreach($defaultData['document_categories'] as $row)
	                  <li id="menu-item-1777" class="menu-item menu-item-type-taxonomy menu-item-object-category mom_default_menu_item active menu-item-depth-1"><a href="{{route('law-and-regulation',['locale'=>$locale,'slug'=>$row->slug])}}">{{$row->title}}</a></li>
	                   @endforeach
	                </ul>
	                <i class="responsive-caret"></i>
	              </li>

                <li id="menu-item-1429" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item mom_default_menu_item menu-item-depth-0 @yield('active-news-and-activities')"><a href="#">{{__('general.news-and-activities')}}</a>
                  <ul class="sub-menu ">
                    @foreach($defaultData['press_categories'] as $row)
                    <li id="menu-item-1777" class="menu-item menu-item-type-taxonomy menu-item-object-category mom_default_menu_item active menu-item-depth-1"><a href="{{route('news-and-activities',['locale'=>$locale,'slug'=>$row->slug])}}">{{$row->title}}</a></li>
                     @endforeach
                  </ul>
                  <i class="responsive-caret"></i>
                </li>

	              
	              <li id="menu-item-1430" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item mom_default_menu_item menu-item-depth-0 @yield('active-report')"><a href="{{route('report',$locale)}}">{{__('general.report')}}</a>
	              </li>
	              <li id="menu-item-1430" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-parent-item mom_default_menu_item menu-item-depth-0 @yield('active-contact-us')"><a href="{{route('contact-us',$locale)}}">{{__('general.contact-us')}}</a>
	              </li>
                
                <li class="language visible-lg" style="">
                  <span style="float:left;">
                    <a href="{{route($defaultData['routeName'], $defaultData['enRouteParamenters'])}}">
                      <img style="margin-top:9px;" src="http://localhost/MPWT/website/lastest/public/frontend/images/mpwt/flag/en.png" class="img img-responsive margin_au">
                    </a>
                  </span> 
                </li>
                <li class="language visible-lg" style="padding-left: 5px;">
                  <span style="float:right;">
                    <a href="{{route($defaultData['routeName'], $defaultData['khRouteParamenters'])}}">
                      <img style="margin-top:9px;" src="http://localhost/MPWT/website/lastest/public/frontend/images/mpwt/flag/kh.png" class="img img-responsive margin_au">
                    </a>
                  </span> 
                </li>
	            </ul>


	          </div> <!--nav inner-->
	      </nav> <!--Navigation-->	 	

	    <div class="boxed-content-wrapper clearfix">  
     		 @yield('content')
         </div> <!--main inner-->
   	</div> <!--content boxed wrapper-->
     

	    <div>  
	        <footer id="footer">
			    <div class="inner" style="">
			      <div class="last" style="">
			        <div class="widget widget_text">
			          <div class="widget-heads">
			            <h4 class="widget-title"><span>{{__('general.contact-us')}}</span></h4>
			          </div>      
			          <div class="textwidget">
			            
			            <div class="recent-post-widget">
			              <ul>
			                <address>
			                 
			                  <p><i class="fa fa-home" aria-hidden="true"></i> {{$defaultData['footer_address']->content }}</p>
			                  <p><i class="fa fa-phone-square" aria-hidden="true"></i> {{$defaultData['footer_phone']->content }}</p>
			                  <p><i class="fa fa-envelope-o" aria-hidden="true"></i> {{$defaultData['footer_email']->content }}</p>
			                  <p><i class="fa fa-internet-explorer" aria-hidden="true"></i> {{$defaultData['footer_website']->content }}</p>
			                </address>
			                 
			              </ul>
			            </div>

			          </div>
			        </div>
			      </div><
			      <div class="clear"></div>                    
			    </div>
			  </footer>
			  <div class="copyrights-area">
			    <div class="inner">
			      <p class="copyrights-text">{{$defaultData['footer_copyright']->content}} </p>
			    </div>
			  </div>
			    <div class="inners" style="float: right; margin-right: 50px;margin-top:-39px;">
			      <p class="copyrights-text">Site By <a href="http://camcyber.com/" style="color: #fffff;">CamCyber</a></p>
			    </div>
			  <div class="clear"></div>
			</div> <!--Boxed wrap-->
			  <a href="#" class="scrollToTop button"><i class="fa fa-angle-double-up"></i></a>
	    </div>

      	
     	

	    <script type="text/javascript" src="{{ asset ('public/frontend/js/jquery.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/jquery-migrate.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/jquery.themepunch.tools.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/jquery.themepunch.revolution.min.js?ver=4.6.9')}}"></script>
		<script type="text/javascript" src="http://news.gdi.gov.kh/wp-includes/js/comment-reply.min.js?ver=4.7.2"></script>
		<script type="text/javascript" src="http://news.gdi.gov.kh/wp-content/plugins/bbpress/templates/default/js/editor.js?ver=2.5.12-6148"></script>
		<script type="text/javascript" src="http://news.gdi.gov.kh/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20"></script>


    <script type="text/javascript" src="http://news.gdi.gov.kh/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.6"></script>
    <script type="text/javascript" src="http://news.gdi.gov.kh/wp-content/themes/goodnews5/js/plugins.js?ver=1.0"></script>
    <script type="text/javascript">
    /* <![CDATA[ */
    var momAjaxL = {"url":"http:\/\/news.gdi.gov.kh\/wp-admin\/admin-ajax.php","nonce":"34ee2e8417","success":"check your email to complete subscription","error":"Already subscribed","error2":"Email invalid","werror":"Enter a valid city name.","nomore":"No More Posts","homeUrl":"http:\/\/news.gdi.gov.kh","viewAll":"View All","noResults":"Sorry, no posts matched your criteria","bodyad":""};
    /* ]]> */
    </script>
    
		<script type="text/javascript" src="{{ asset ('public/frontend/js/main.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/jquery.prettyPhoto.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/wp-embed.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/js_composer_front.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/core.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/widget.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/tabs.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/jquery-ui-tabs-rotate.js')}}"></script>
		<script type="text/javascript" src="{{ asset ('public/frontend/js/jssor.slider-22.2.6.min.js')}}"></script>
		<script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 4,
              $SlideDuration: 2500,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Cols: 7
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1235);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>
     	<script>
            function init() {
            var imgDefer = document.getElementsByTagName('img');
            for (var i=0; i<imgDefer.length; i++) {
            if(imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
            } } }
            window.onload = init;
      	</script>
      @yield('appbottomjs')

  </body>
</html>

