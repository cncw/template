@extends('cp.layouts.master')

@section ('headercss')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset ('public/cp/css/lib/summernote/summernote.css') }}"/>
    <link rel="stylesheet" href="{{ asset ('public/cp/css/main.css') }}">
    <script type="text/javascript" src="{{ asset ('public/cp/js/lib/jquery/jquery.min.js') }}"></script>
    @yield('appheadercss')
@endsection



@section ('bodyclass')
    class="with-side-menu control-panel control-panel-compact"
@endsection

@section ('header')

<header class="site-header">
    <div class="container-fluid">
        <a target="_blank" href="{{ url('/') }}" class="site-logo">
            <img style="width: 210px;" class="hidden-md-down" src="{{ asset ('public/cp/img/banner.jpg') }}" alt="">
            <img class="hidden-lg-up" src="{{ asset ('public/cp/img/logo.png') }}" alt="">
        </a>
        <button class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">
                    
                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset (Auth::user()->avatar) }}" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="{{ route('cp.user.profile.edit') }}"><span class="fa fa-user"></span> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('cp.auth.logout') }}"><span class="fa fa-sign-out"></span> Logout</a>
                        </div>
                    </div>

                    <button type="button" class="burger-right">
                        <i class="font-icon-menu-addl"></i>
                    </button>
                </div><!--.site-header-shown-->

                <div class="mobile-menu-right-overlay"></div>
                
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->
@endsection

@section ('menu')
    @php ($menu = "")
    @if(isset($_GET['menu']))
        @php( $menu = $_GET['menu'])
    @endif
    

    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu">
        <ul class="side-menu-list">   
            <li class="red @yield('active-main-menu-slide')">
                <a href="{{ route('cp.slide.index') }}">
                <span>
                    <i class="fa fa-desktop"></i>
                    <span class="lbl">Slide</span>
                </span>
                </a>
            </li>
           
            <li class="@yield('active-main-menu-general') red with-sub">
                <span>
                    <i class=" font-icon fa fa-home"></i>
                    <span class="lbl">General</span>
                </span>
                <ul>
                   
                    <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'message-from-prie-minister']) }}?menu=general"><span class="lbl">Messase From Prire Minister</span></a></li>
                     <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'message-from-minister']) }}?menu=general"><span class="lbl">Messase From Minister</span></a></li>                                                  
                </ul>
            </li>
            <li class="@yield('active-main-menu-about-us') red with-sub">
                <span>
                    <i class=" font-icon fa fa-info"></i>
                    <span class="lbl">About Us</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'history']) }}?menu=about-us"><span class="lbl">History</span></a></li>
                    <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'mission']) }}?menu=about-us"><span class="lbl">Mission</span></a></li> 

                    <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'vision']) }}?menu=about-us"><span class="lbl">Vision</span></a></li>                                                  
                </ul>
            </li> 
            <li class="red @yield('active-main-menu-law')">
                <a href="{{ route('cp.law.index') }}">
                <span>
                    <i class="fa fa-gavel"></i>
                    <span class="lbl">Law and Regulation</span>
                </span>
                </a>
            </li>

            <li class="@yield('active-main-menu-news') red with-sub">
                <span>
                    <i class=" font-icon fa fa-newspaper-o"></i>
                    <span class="lbl">News & Activities</span>
                </span>
                <ul>
                    <li class=""><a href="{{ route('cp.news.index') }}"><span class="lbl">News</span></a></li> 
                    <li class=""><a href="{{ route('cp.activity.index') }}"><span class="lbl">Activities</span></a></li> 
                    <li class=""><a href="{{ route('cp.announcement.index') }}"><span class="lbl">Announcement</span></a></li> 
                                                                 
                </ul>
            </li>

            <li class="red @yield('active-main-menu-video')">
                <a href="{{ route('cp.video.index') }}">
                <span>
                    <i class="fa fa-video-camera"></i>
                    <span class="lbl">Video</span>
                </span>
                </a>
            </li>

            <li class="red @yield('active-main-menu-report')">
                <a href="{{ route('cp.report.index') }}">
                <span>
                    <i class="fa fa-file"></i>
                    <span class="lbl">Report</span>
                </span>
                </a>
            </li>
            
            <li class="red @yield('active-main-menu-message')">
                <a href="{{ route('cp.message.index') }}">
                <span>
                    <i class="fa fa-envelope"></i>
                    <span class="lbl">Message</span>
                </span>
                </a>
            </li>
            <li class="@yield('active-main-menu-contact-us') red with-sub">
                <span>
                    <i class=" font-icon fa fa-home"></i>
                    <span class="lbl">Contact Content</span>
                </span>
                <ul>
                   
                    <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'address-contact']) }}?menu=contact-us"><span class="lbl">Address</span></a></li>
                     <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'phone-contact']) }}?menu=contact-us"><span class="lbl">Phone</span></a></li> <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'email-contact']) }}?menu=contact-us"><span class="lbl">Email</span></a></li>  
                     <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'website-contact']) }}?menu=contact-us"><span class="lbl">Website</span></a></li>                                               
                </ul>
            </li>

            <li class="@yield('active-main-menu-footer') red with-sub">
                <span>
                    <i class=" font-icon fa fa-home"></i>
                    <span class="lbl">Footer Content</span>
                </span>
                <ul>
                   
                    <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'address']) }}?menu=footer"><span class="lbl">Address</span></a></li>
                     <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'phone']) }}?menu=footer"><span class="lbl">Phone</span></a></li> <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'email']) }}?menu=footer"><span class="lbl">Email</span></a></li>  
                     <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'website']) }}?menu=footer"><span class="lbl">Website</span></a></li>
                     <li class=""><a href="{{ route('cp.content.content.edit', ['slug' => 'copyright']) }}?menu=footer"><span class="lbl">Copyright</span></a></li>                                               
                </ul>
            </li>
            <li class="red @yield('active-main-menu-image')">
                <a href="{{ route('cp.image.index') }}">
                <span>
                    <i class="fa fa-desktop"></i>
                    <span class="lbl">Image</span>
                </span>
                </a>
            </li>
            @if(Auth::user()->position_id == 1)
             <li class="red @yield('active-main-menu-user')">
                <a href="{{ route('cp.user.user.index') }}">
                <span>
                    <i class="fa fa-users"></i>
                    <span class="lbl">Users</span>
                </span>
                </a>
            </li>
            @endif

           
        </ul>
    </nav><!--.side-menu-->

@endsection

@section ('content')
    <div class="page-content">
        
        @yield ('page-content')
        
    </div>
@endsection




@section ('bottomjs')
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        @yield ('imageuploadjs')
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/tether/tether.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/bootstrap/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/plugins.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/lobipanel/lobipanel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset ('public/cp/js/lib/blockUI/jquery.blockUI.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset ('public/cp/js/lib/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{ asset ('public/cp/js/lib/select2/select2.full.min.js')}}"></script>
       <script type="text/javascript" src="{{ asset ('public/cp/js/lib/summernote/summernote.min.js') }}"></script>
        <script src="{{ asset ('public/cp/js/app.js') }}"></script>
        <script src="{{ asset ('public/cp/js/camcyber.js') }}"></script>
        @yield('appbottomjs')

        @if(Session::has('msg'))
        <script type="text/JavaScript">
            toastr.success("{!!Session::get('msg')!!}");
        </script>
        @endif
        @if(Session::has('error'))
        <script type="text/JavaScript">
            toastr.error("{!!Session::get('error')!!}");
        </script>
        @endif
@endsection